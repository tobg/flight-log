/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.about

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.oss.licenses.OssLicensesMenuActivity
import de.tobiasgrundmann.flightlog.R
import de.tobiasgrundmann.flightlog.databinding.FragmentAboutBinding
import de.tobiasgrundmann.flightlog.extensions.resetBackStackHandler

class AboutFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: FragmentAboutBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_about, container, false)

        resetBackStackHandler()
        val viewModelFactory =
            AboutViewModelFactory()
        val viewModel = ViewModelProvider(
            this, viewModelFactory
        ).get(AboutViewModel::class.java)
        binding.aboutViewModel = viewModel

        viewModel.navigateToLicensesActivity.observe(viewLifecycleOwner) {
            if (it == true) {
                startActivity(Intent(requireContext(), OssLicensesMenuActivity::class.java))
                viewModel.doneNavigating()
            }
        }
        setHasOptionsMenu(true)
        return binding.root

    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.clear()
        super.onPrepareOptionsMenu(menu)
    }

}
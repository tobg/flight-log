/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.extensions

import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import de.tobiasgrundmann.flightlog.MainActivity

fun Fragment.resetBackStackHandler() {
    //Reinstate backstack handling which was modified to handle the slidingPane
    val toolbar = (requireActivity() as MainActivity).binding.toolbar
    toolbar.setNavigationOnClickListener {
        //parentFragmentManager.popBackStack() doesn't work on android 11
        NavigationUI.navigateUp(
            requireView().findNavController(), null
        )
    }
}

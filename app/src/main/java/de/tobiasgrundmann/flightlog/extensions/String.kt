/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.extensions

import android.text.Spannable
import android.text.SpannableString
import android.text.style.UnderlineSpan
import java.text.Normalizer
import kotlin.math.min

private val diacritical = "\\p{InCombiningDiacriticalMarks}+".toRegex()
fun String.searchableWithoutDiacritics(): String {
    return Normalizer.normalize(this, Normalizer.Form.NFD).replace(diacritical, "")
}

fun String.highlightText(highlight: String?): SpannableString {
    if (highlight.isNullOrEmpty()) {
        return SpannableString(this)
    }
    val spannableString = SpannableString(this)
    val nText = this.searchableWithoutDiacritics()
    val nHighlight = highlight.searchableWithoutDiacritics()
    var startIndex = nText.indexOf(nHighlight, 0, true)
    val indexes = ArrayList<Int>()
    while (startIndex >= 0) {
        indexes.add(startIndex)
        startIndex = nText.indexOf(nHighlight, startIndex + nHighlight.length, true)
    }
    indexes.forEach {
        val endIndex = min(it + nHighlight.length, nText.length)
        spannableString.setSpan(
            UnderlineSpan(),
            it,
            endIndex,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
    //Timber.d("highlight [$spannableString] [$nHighlight]")
    return spannableString
}
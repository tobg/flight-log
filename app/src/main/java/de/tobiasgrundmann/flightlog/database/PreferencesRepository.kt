/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.database

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import timber.log.Timber
import java.io.IOException
import java.net.URL
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets

data class UserPreferences(
    val importExportCharset: Charset = StandardCharsets.ISO_8859_1,
    val pilotImportUrl: URL? = null
)

private object PreferencesKeys {
    val IMPORT_EXPORT_CHARSET = stringPreferencesKey("import_export_charset")
    val IMPORT_PILOT_URL = stringPreferencesKey("pilot_import_url")

}

class PreferencesRepository(private val preferencesStore: DataStore<Preferences>) {
    val userPreferencesFlow: Flow<UserPreferences> = preferencesStore.data
        .catch { exception ->
            if (exception is IOException) {
                Timber.e(exception)
                emit(emptyPreferences())
            } else {
                throw exception
            }
        }
        .map { preferences ->
            val importExport = preferences[PreferencesKeys.IMPORT_EXPORT_CHARSET]
            val charset = if (importExport != null) {
                Charset.forName(importExport)
            } else {
                StandardCharsets.ISO_8859_1
            }
            val pilotImportUrlStr = preferences[PreferencesKeys.IMPORT_PILOT_URL]

            val pilotImportUrl = if (!pilotImportUrlStr.isNullOrEmpty()) {
                kotlin.runCatching {
                    URL(pilotImportUrlStr)
                }.getOrNull()
            } else {
                null
            }
            UserPreferences(
                importExportCharset = charset, pilotImportUrl = pilotImportUrl
            )
        }

    suspend fun updateImportExportCharset(charset: Charset) {
        preferencesStore.edit { preferences ->
            preferences[PreferencesKeys.IMPORT_EXPORT_CHARSET] = charset.name()
        }
    }

    suspend fun updatePilotImportUrl(url: URL?) {
        preferencesStore.edit { preferences ->
            preferences[PreferencesKeys.IMPORT_PILOT_URL] = url?.toString() ?: ""
        }
    }
}
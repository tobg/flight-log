/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.database

//import java.sql.Date
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import de.tobiasgrundmann.flightlog.common.Activatable
import java.util.*

@Entity(
    tableName = "pilots",
    indices = [Index(
        value = ["firstname", "lastname"]
    ),Index(
        value = ["customer_id"],
        unique = true
    )]
)
data class Pilot(
    @ColumnInfo(name = "firstname") var firstname: String = "",
    @ColumnInfo(name = "lastname") var lastname: String = "",
    @ColumnInfo(name = "weight") var weight: Int = 0,
    @ColumnInfo(name = "customer_id") var customerID: String? = null,

    //to be copied to flight for every new flight
    @ColumnInfo(name = "status") var status: String = "",
    @ColumnInfo(name = "current_flight_vehicle") var currentFlightVehicle: String = "",

    //@ColumnInfo(name = "birthday") var birthday: Date?,

    //there is a room issue with identical column-names on joins for multimaps
    @ColumnInfo(name = "active") var active: Boolean = true,
    @PrimaryKey
    @ColumnInfo(name = "p_uid")
    val pUid: UUID = UUID.randomUUID()
) : Activatable {
    override fun uuid(): UUID = pUid
    override fun activate() {
        active = true
    }

    override fun deactivate() {
        active = false
    }

    override fun toggleActivation() : Boolean {
        active = !active
        return active
    }

    override fun activated(): Boolean = active
}
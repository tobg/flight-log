/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.database

import android.net.Uri
import androidx.core.content.FileProvider
import androidx.lifecycle.MutableLiveData
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.dataformat.csv.CsvMapper
import com.fasterxml.jackson.dataformat.csv.CsvParser
import com.fasterxml.jackson.dataformat.csv.CsvSchema
import com.fasterxml.jackson.module.kotlin.KotlinModule
import de.tobiasgrundmann.flightlog.FlightLogApplication
import de.tobiasgrundmann.flightlog.MessageId
import de.tobiasgrundmann.flightlog.MessageToUser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.BufferedOutputStream
import java.io.File
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.nio.charset.Charset
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.util.*


class Exporter(private val application: FlightLogApplication) {
    private val dtFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss_")
    private val preferences = application.preferencesRepository.userPreferencesFlow

    @JsonPropertyOrder(
        "customerID",
        "firstname",
        "lastname",
        "starttime",
        "landingtime",
        "landingConfirmed",
        "flyingVehicle",
        "status",
        "scenario",
        "scenAttr0",
        "scenAttr1",
        "scenAttr2",
        "flightUUID",
    )
    data class ExportCSV(
        val firstname: String = "",
        val lastname: String = "",
        val customerID: String? = null,
        val starttime: String = "",
        val landingtime: String = "",
        val landingConfirmed: String = "",
        val flyingVehicle: String = "",
        val status: String = "",
        val scenario: String = "",
        val scenAttr0: String = "",
        val scenAttr1: String = "",
        val scenAttr2: String = "",
        val flightUUID: String = ""
    )


    suspend fun exportPilots(
        exportURI: MutableLiveData<Uri>,
        pilotsUuids: Set<UUID>,
    ) {
        val charset: Charset = preferences.first().importExportCharset
        val csvMapper = CsvMapper().apply {
            registerModule(
                KotlinModule.Builder()
                    .build()
            )
            disable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY)
        }
        val uri = withContext(Dispatchers.IO) {
            deleteCache()
            kotlin.runCatching {
                val uri = createExportFile("pilots_export")
                val pilots = application.repository.getPilotsForExport(pilotsUuids)
                application.contentResolver.openOutputStream(uri).use { stream ->
                    val writer = csvMapper.writer(
                        csvMapper.schemaFor(PilotCSV::class.java)
                            .withHeader()
                    ).writeValues(OutputStreamWriter(BufferedOutputStream(stream), charset))
                    pilots.forEach { pilot ->
                        writer.write(PilotCSV(pilot))
                    }
                }
                uri
            }.getOrThrow()
        }
        exportURI.postValue(uri)
    }

    suspend fun exportFlights(
        exportURI: MutableLiveData<Uri>,
        pilotsUuids: Set<UUID>,
        dateRange: Pair<OffsetDateTime?, OffsetDateTime?>
    ) {
        val charset: Charset = preferences.first().importExportCharset
        val csvMapper = CsvMapper().apply {
            registerModule(KotlinModule.Builder().build())
        }
        val uri = withContext(Dispatchers.IO) {
            deleteCache()
            kotlin.runCatching {
                val uri = createExportFile("flights_export")
                val fps = if (dateRange.first == null || dateRange.second == null) {
                    if (pilotsUuids.isNotEmpty()) {
                        application.repository.getPilotsWithFlights(pilotsUuids)
                    } else {
                        application.repository.getAllPilotsWithFlights()
                    }
                } else {
                    val rangeEnd = dateRange.second!!.plusDays(1)
                    Timber.d("rangeEnd adjusted: [$rangeEnd]")
                    if (pilotsUuids.isNotEmpty()) {
                        application.repository.getPilotsWithFlights(
                            pilotsUuids,
                            dateRange.first!!,
                            rangeEnd
                        )
                    } else {
                        application.repository.getAllPilotsWithFlights(
                            dateRange.first!!,
                            rangeEnd
                        )
                    }
                }

                application.contentResolver.openOutputStream(uri).use { stream ->
                    val writer = csvMapper.writer(
                        csvMapper.schemaFor(ExportCSV::class.java)
                            .withHeader()
                    ).writeValues(OutputStreamWriter(BufferedOutputStream(stream), charset))
                    fps.entries.forEach { (pilot, flights) ->
                        flights.forEach { flight ->
                            val landingTime = if (flight.landingTime != null) {
                                flight.landingTime?.toZonedDateTime().toString()
                            } else {
                                ""
                            }
                            val scenarioUid = flight.scenarioUid
                            val scenarioWithAttributes = if (scenarioUid != null) {
                                //TODO transaction or single query
                                application.repository.getScenarioWithAttributes(scenarioUid)
                            } else {
                                FlightLogRepository.emptyScenarioWithAttributes
                            }
                            val scenario = scenarioWithAttributes.keys.elementAt(0)
                            val scenarioName = scenario.name
                            val scenarioAttr0 =
                                scenarioWithAttributes[scenario]?.get(0)?.value ?: ""
                            val scenarioAttr1 =
                                scenarioWithAttributes[scenario]?.get(1)?.value ?: ""
                            val scenarioAttr2 =
                                scenarioWithAttributes[scenario]?.get(2)?.value ?: ""

                            writer.write(
                                ExportCSV(
                                    pilot.firstname,
                                    pilot.lastname,
                                    pilot.customerID,
                                    flight.startTime.toZonedDateTime().toString(),
                                    landingTime,
                                    flight.landingConfirmed.toString(),
                                    flight.vehicle,
                                    flight.status,
                                    scenarioName,
                                    scenarioAttr0,
                                    scenarioAttr1,
                                    scenarioAttr2,
                                    flight.fUid.toString()
                                )
                            )
                        }
                    }

                }
                uri
            }.getOrThrow()
        }
        exportURI.postValue(uri)
    }

    private fun createExportFile(namepart: String): Uri {
        val df = OffsetDateTime.now().format(dtFormatter)

        val file =
            File.createTempFile(
                "${namepart}_$df",
                ".csv",
                application.applicationContext.cacheDir
            )
        file.createNewFile()
        return FileProvider.getUriForFile(
            application.applicationContext,
            "de.tobiasgrundmann.flightlog.fileprovider",
            file
        )
    }

    private fun deleteCache() {
        val cacheDir = application.applicationContext.cacheDir
        cacheDir?.listFiles()?.forEach {
            it.delete()
        }
    }

    data class PilotCSV(
        val customerID: String? = null,
        val firstname: String = "",
        val lastname: String = "",
        //Use double as exchange format since this is more universal
        //When importing files with integers or double both will parse
        //Export as double shouldn't cause any problems either
        val weight: Double = 0.0,
        val status: String = "",
        val currentFlightVehicle: String = "",
    ) {
        constructor(pilot: Pilot) : this(
            pilot.customerID,
            pilot.firstname,
            pilot.lastname,
            pilot.weight.toDouble(),
            pilot.status,
            pilot.currentFlightVehicle
        )

        private var sanitized = false
        private fun sanitize(str: String): String {
            return str.trim()
        }

        fun sanitize(): PilotCSV {
            val sanitizedCustomerId = if (customerID.isNullOrBlank()) {
                null
            } else {
                sanitize(customerID)
            }
            val sp = PilotCSV(
                sanitizedCustomerId,
                sanitize(firstname),
                sanitize(lastname),
                weight,
                sanitize(status),
                sanitize(currentFlightVehicle),
            )
            sp.sanitized = true
            return sp
        }

        fun createPilot(): Pilot {
            assert(sanitized)
            return Pilot(
                firstname,
                lastname,
                weight.toInt(),
                customerID,
                status,
                currentFlightVehicle,
                false
            )
        }

        fun updatePilot(pilot: Pilot) {
            assert(sanitized)
            assert(pilot.customerID!!.trim() == customerID)
            val cId = pilot.customerID
            if (cId != null && cId.trim() == customerID) {
                pilot.firstname = firstname
                pilot.lastname = lastname
                pilot.weight = weight.toInt()
                pilot.status = status
                pilot.currentFlightVehicle = currentFlightVehicle
            } else {
                Timber.e("Attempt to update pilot with customerid [${pilot.customerID}] from [{$customerID}] failed")
            }
        }
    }

    suspend fun importPilots(uri: Uri, errorMsg: MutableLiveData<MessageToUser>) {
        Timber.d("importPilots [$uri]")
        val charset: Charset = preferences.first().importExportCharset
        val csvMapper = CsvMapper().apply {
            registerModule(KotlinModule.Builder().build())

        }
        val schema = CsvSchema.builder()
            .addColumn("customerID")
            .addColumn("firstname")
            .addColumn("lastname")
            .addColumn("weight", CsvSchema.ColumnType.NUMBER)
            .addColumn("status")
            .addColumn("currentFlightVehicle")
            .build()
        val pilotCsv = withContext(Dispatchers.IO) {
            kotlin.runCatching {
                application.contentResolver.openInputStream(uri).use { stream ->
                    csvMapper
                        .readerFor(PilotCSV::class.java)
                        .with(schema.withHeader())
                        .with(CsvParser.Feature.IGNORE_TRAILING_UNMAPPABLE)
                        .readValues<PilotCSV>(InputStreamReader(stream, charset))
                        .readAll()
                        .toList()
                }
            }.getOrElse {
                errorMsg.postValue(
                    MessageToUser(
                        MessageId.IMPORT_ERROR,
                        arrayOf(it.message ?: it.javaClass.canonicalName)
                    )
                )
                listOf()
            }
        }

        pilotCsv.forEach {
            Timber.d("read for import [$it]")
        }
        application.repository.importPilots(pilotCsv.map { csv -> csv.sanitize() })
    }

    suspend fun importPilotsFromServer(errorMsg: MutableLiveData<MessageToUser>) {
        withContext(Dispatchers.IO) {
            val url = preferences.first().pilotImportUrl
            val charset: Charset = preferences.first().importExportCharset
            Timber.d("importPilotsFromServer [$url]")
            if (url != null) {

                kotlin.runCatching {
                    val pilotsImport = url.readText(charset)
                    Timber.d(pilotsImport)
                    val uri = createExportFile("pilots_import")
                    application.contentResolver.openOutputStream(uri)?.bufferedWriter(charset)
                        .use { writer ->
                            writer?.append(pilotsImport)
                            writer?.flush()
                        }
                    importPilots(uri, errorMsg)
                }.onFailure {
                    errorMsg.postValue(
                        MessageToUser(
                            MessageId.IMPORT_ERROR,
                            arrayOf(it.message ?: it.javaClass.canonicalName)
                        )
                    )
                }
                deleteCache()
            } else {
                errorMsg.postValue(
                    MessageToUser(
                        MessageId.CONFIGURE_IMPORT_URL
                    )
                )
            }
        }
    }
}


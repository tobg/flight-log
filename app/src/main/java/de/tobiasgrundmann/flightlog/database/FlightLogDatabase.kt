/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.OffsetDateTime
import kotlin.random.Random

@Database(
    entities = [Pilot::class, Flight::class, Scenario::class, GenericScenarioAttribute::class],
    version = 2
)
@TypeConverters(Converter::class)
abstract class FlightLogDatabase : RoomDatabase() {

    abstract fun dao(): FlightLogDao

    companion object {
        @Volatile
        private var INSTANCE: FlightLogDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): FlightLogDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    FlightLogDatabase::class.java,
                    "shopping_list_database"
                ).fallbackToDestructiveMigration()
                    .addCallback(FlightLogCallback(context, scope))
                    .build()
                INSTANCE = instance
                instance
            }
        }

        private class FlightLogCallback(
            private val context: Context,
            private val scope: CoroutineScope
        ) : RoomDatabase.Callback() {

            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                INSTANCE?.let { database ->
                    scope.launch(Dispatchers.IO) {
                        populateDatabase(database.dao())
                    }
                }
            }
            var counter = 0
            fun createSample(firstName: String): Pilot {
                val lastname = arrayOf("Hinz", "Kunz")[(0..1).random()]
                val status = arrayOf("Guest", "Club", "Student")[(0..2).random()]
                val customerId = if (Random.nextBoolean()) {
                    "${counter++}_A_${(1000..9999).random()}"
                } else {
                    null
                }
                val weight = (50..100).random()
                val active = Random.nextBoolean()
                val vehicle =
                    arrayOf("Paraglider", "Hangglider", "Bi-Place Paraglider")[(0..2).random()]
                return Pilot(firstName, lastname, weight, customerId, status, vehicle, active)
            }

            suspend fun populateDatabase(dao: FlightLogDao) {
                dao.deleteAllPilots()
                dao.deleteAllScenarios()

                val pilots = listOf(
                    createSample("Otto"),
                    createSample("Anna"),
                    createSample("Berta"),
                    createSample("Frieda"),
                    createSample("Friedolin"),
                    createSample("Hans"),
                    createSample("Jack"),
                )
                val allFlights: MutableList<Flight> = mutableListOf()
                val start = OffsetDateTime.now()
                pilots.forEach { pilot ->
                    dao.insert(pilot)
                    val flights: MutableList<Flight> = mutableListOf(
                        Flight(pilot, start),
                        Flight(
                            pilot,
                            start.minusHours((1L..2L).random()).minusMinutes((0L..59L).random())
                        ),
                        Flight(
                            pilot,
                            start.minusHours((3L..5L).random()).minusMinutes((0L..59L).random())
                        ),
                        Flight(
                            pilot,
                            start.minusDays((1L..3L).random()).minusMinutes((0L..59L).random())
                        ),
                        Flight(
                            pilot,
                            start.minusDays((4L..5L).random()).minusMinutes((0L..59L).random())
                        ),
                        Flight(
                            pilot,
                            start.minusDays((6L..20L).random()).minusMinutes((0L..59L).random())
                        )
                    )
                    flights.forEach { flight ->  flight.landingConfirmed = true}
                    flights.first().landingConfirmed = false
                    allFlights.addAll(flights)
                    flights.forEach {
                        dao.insert(it)
                    }
                }

                val scenarios = listOf(
                    Scenario("Altes Lager W"),
                    Scenario("Altes Lager School"),
                    Scenario("Cottbus"),
                    Scenario("Algodonales"),
                    Scenario("EDBX")
                )
                scenarios.forEach { scenario ->
                    dao.insert(scenario)
                    dao.insert(GenericScenarioAttribute(scenario, "attr0", "WF: Frieda"))
                    dao.insert(GenericScenarioAttribute(scenario, "attr1", "Start: Berthold (Assistent)"))
                    dao.insert(GenericScenarioAttribute(scenario, "attr2", "Landing: Anna (Teacher)"))
                }
                dao.setSingleScenarioActive(scenarios.first().sUid)

                allFlights.forEach { flight ->
                    flight.scenarioUid = scenarios[scenarios.indices.random()].sUid
                    dao.update(flight)
                }
            }
        }
    }
}
/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.database

import androidx.room.*
import java.time.OffsetDateTime
import java.util.*

@Entity(
    tableName = "flights",
    indices = [
        Index(
            value = ["pilot_uid"],
            unique = false
        ),
        Index(
            value = ["scenario_uid"],
            unique = false
        )],
    foreignKeys = [
        ForeignKey(
            entity = Pilot::class,
            parentColumns = arrayOf("p_uid"),
            childColumns = arrayOf("pilot_uid"),
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Scenario::class,
            parentColumns = arrayOf("s_uid"),
            childColumns = arrayOf("scenario_uid"),
            onDelete = ForeignKey.SET_NULL
        )]
)
data class Flight(
    @ColumnInfo(name = "pilot_uid") val pilotUid: UUID,
    @ColumnInfo(name = "start_time") var startTime: OffsetDateTime,
    @ColumnInfo(name = "landing_time") var landingTime: OffsetDateTime? = null,
    @ColumnInfo(name = "landing_confirmed") var landingConfirmed: Boolean = false,

    //copied from pilot.status
    @ColumnInfo(name = "status") var status: String = "",
    @ColumnInfo(name = "flight_vehicle") var vehicle: String = "",

    @ColumnInfo(name = "scenario_uid") var scenarioUid: UUID? = null,

    //there is a room issue with identical column-names on joins for multimaps
    @PrimaryKey
    @ColumnInfo(name = "f_uid")
    val fUid: UUID = UUID.randomUUID()
) {
    constructor(pilot: Pilot, startTime: OffsetDateTime) : this(
        pilot.pUid,
        startTime,
        status = pilot.status,
        vehicle = pilot.currentFlightVehicle
    )

}
/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.database

import androidx.lifecycle.LiveData
import androidx.room.*
import kotlinx.coroutines.flow.Flow
import java.time.OffsetDateTime
import java.util.*

@Dao
abstract class FlightLogDao {

    @Query("SELECT * FROM scenarios WHERE active = 1 LIMIT 1")
    abstract fun activeScenario(): LiveData<Scenario>

    @Query("SELECT * FROM scenarios WHERE name = :name")
    abstract fun getScenario(name: String): Scenario?

    @Query("SELECT s_uid FROM scenarios WHERE active = 1 LIMIT 1")
    abstract fun activeScenarioUid(): UUID

    @Query("DELETE FROM pilots WHERE p_uid IN (:uuids)")
    abstract suspend fun deletePilots(uuids: Set<UUID>)

    @Query("DELETE FROM scenarios WHERE s_uid IN (:uuids)")
    abstract suspend fun deleteScenarios(uuids: Set<UUID>)

    @Query("DELETE FROM flights WHERE f_uid IN (:uuids)")
    abstract suspend fun deleteFlights(uuids: Set<UUID>)

    @Query("DELETE FROM pilots")
    abstract suspend fun deleteAllPilots()

    @Query("DELETE FROM scenarios")
    abstract suspend fun deleteAllScenarios()

    @Query("SELECT * FROM pilots ORDER BY active DESC,firstname COLLATE LOCALIZED, lastname COLLATE LOCALIZED")
    abstract fun getPilots(): Flow<List<Pilot>>

    @Query("SELECT * FROM pilots ORDER BY lastname COLLATE LOCALIZED, firstname COLLATE LOCALIZED")
    abstract fun getPilotsForExport(): List<Pilot>

    @Query(
        "SELECT * FROM pilots "
                + " WHERE p_uid in (:uuids)"
                + "ORDER BY lastname COLLATE LOCALIZED, firstname COLLATE LOCALIZED"
    )
    abstract suspend fun getPilotsForExport(uuids: Set<UUID>): List<Pilot>

    @Query("SELECT * FROM scenarios ORDER BY active DESC,name COLLATE LOCALIZED")
    abstract fun getScenarios(): Flow<List<Scenario>>

    @Query(
        "SELECT * FROM scenarios as s"
                + " JOIN generic_scenario_attributes as a ON s.s_uid = a.scenario_uid"
                + " ORDER BY active DESC,name COLLATE LOCALIZED"
    )
    abstract fun getScenariosWithAttributes(): Flow<Map<Scenario, List<GenericScenarioAttribute>>>

    @Query(
        "SELECT * FROM scenarios as s"
                + " JOIN generic_scenario_attributes as a ON s.s_uid = a.scenario_uid"
                + " WHERE s.s_uid = :scenarioUUID"
                + " ORDER BY active DESC,name COLLATE LOCALIZED"
    )
    abstract suspend fun getScenarioWithAttributes(scenarioUUID: UUID): Map<Scenario, List<GenericScenarioAttribute>>

    @Query(
        "SELECT * FROM pilots "
                + " JOIN flights ON pilots.p_uid = flights.pilot_uid"
                + " WHERE pilot_uid in (:pilotUuids)"
                + " ORDER BY datetime(start_time) DESC"
    )
    abstract suspend fun getPilotsWithFlights(pilotUuids: Set<UUID>): Map<Pilot, List<Flight>>

    @Query(
        "SELECT * FROM pilots "
                + " JOIN flights ON pilots.p_uid = flights.pilot_uid"
                + " ORDER BY datetime(start_time) DESC"
    )
    abstract suspend fun getAllPilotsWithFlights(): Map<Pilot, List<Flight>>

    @Query(
        "SELECT * FROM pilots "
                + " JOIN flights ON pilots.p_uid = flights.pilot_uid"
                + " WHERE pilot_uid in (:pilotUuids)"
                + " AND datetime(start_time) BETWEEN datetime(:rangeStart) AND datetime(:rangeEnd)"
                + " ORDER BY datetime(start_time) DESC"
    )
    abstract suspend fun getPilotsWithFlights(
        pilotUuids: Set<UUID>,
        rangeStart: OffsetDateTime,
        rangeEnd: OffsetDateTime
    ): Map<Pilot, List<Flight>>

    @Query(
        "SELECT * FROM pilots "
                + " JOIN flights ON pilots.p_uid = flights.pilot_uid"
                + " AND datetime(start_time) BETWEEN datetime(:rangeStart) AND datetime(:rangeEnd)"
                + " ORDER BY datetime(start_time) DESC"
    )
    abstract suspend fun getAllPilotsWithFlights(
        rangeStart: OffsetDateTime,
        rangeEnd: OffsetDateTime
    ): Map<Pilot, List<Flight>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract suspend fun insert(pilot: Pilot)

    @Update
    abstract suspend fun update(pilot: Pilot)

    @Query("SELECT * FROM pilots where p_uid = :pilotUID")
    abstract fun getPilot(pilotUID: UUID): LiveData<Pilot>

    @Query("SELECT * FROM pilots where customer_id = :customerId")
    abstract fun getPilot(customerId: String): Pilot?

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract suspend fun insert(flight: Flight)

    @Update
    abstract suspend fun update(flight: Flight)

    @Query("SELECT * FROM flights WHERE pilot_uid = :pilotUid ORDER BY datetime(start_time) DESC")
    abstract fun getFlights(pilotUid: UUID): Flow<List<Flight>>

//    @Transaction
//    @Query("SELECT * FROM flights WHERE pilot_uid = :pilotUid ORDER BY datetime(start_time) DESC")
//    abstract fun getFlightsAndScenarios(pilotUid: UUID): Flow<List<FlightAndScenario>>

    @Query("SELECT * FROM flights LEFT JOIN scenarios on flights.scenario_uid = scenarios.s_uid WHERE pilot_uid = :pilotUid ORDER BY datetime(start_time) DESC")
    abstract fun getFlightsAndScenarios(pilotUid: UUID): Flow<List<FlightAndScenario>>

    @Query("UPDATE pilots  SET active = :status WHERE p_uid IN (:uuids)")
    abstract suspend fun setActive(uuids: Set<UUID>, status: Boolean)

    @Query("UPDATE scenarios  SET active = CASE s_uid WHEN :uuid THEN 1 ELSE 0 END")
    abstract suspend fun setSingleScenarioActive(uuid: UUID)

    @Query("UPDATE flights SET scenario_uid = (SELECT s_uid FROM SCENARIOS s where s.active = 1 LIMIT 1) where f_uid in (:uuids)")
    abstract suspend fun setActiveScenarioToFlights(uuids: Set<UUID>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract suspend fun insert(scenario: Scenario)

    @Update
    abstract suspend fun update(scenario: Scenario)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract suspend fun insert(attr: GenericScenarioAttribute)

    @Update
    abstract suspend fun update(attr: GenericScenarioAttribute)

    @Transaction
    open suspend fun transactional(tx: suspend () -> Unit) = tx()

    @Query(
        """
        SELECT p.*,fl.* FROM pilots p
        INNER JOIN 
         (SELECT fa.* FROM flights fa
             INNER JOIN (
                 SELECT pilot_uid, MAX(datetime(start_time)), start_time
                 FROM flights 
             ) b ON fa.pilot_uid = b.pilot_uid AND fa.start_time = b.start_time
         ) fl ON p_uid = fl.pilot_uid
              """
    )
    abstract fun lastStartedPilot(): Flow<Map<Pilot,Flight>>

}

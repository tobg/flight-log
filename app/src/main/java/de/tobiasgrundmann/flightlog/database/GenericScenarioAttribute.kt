/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.database

import androidx.room.*
import java.util.*

@Entity(
    tableName = "generic_scenario_attributes",
    indices = [Index(
        value = ["scenario_uid"],
        unique = false
    )],
    foreignKeys = [ForeignKey(
        entity = Scenario::class,
        parentColumns = arrayOf("s_uid"),
        childColumns = arrayOf("scenario_uid"),
        onDelete = ForeignKey.CASCADE
    )]
)
data class GenericScenarioAttribute(
    @ColumnInfo(name = "scenario_uid") val scenarioUid: UUID,
    @ColumnInfo(name = "key") var key: String = "",
    @ColumnInfo(name = "value") var value: String = "",

    @PrimaryKey
    @ColumnInfo(name = "gsa_uid")
    //there is a room issue with identical column-names on joins for multimaps
    val gsaUid: UUID = UUID.randomUUID()
) {
    constructor(scenario: Scenario, key: String, value: String) : this(
        scenario.sUid, key, value
    )
}
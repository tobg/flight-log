/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.database

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import de.tobiasgrundmann.flightlog.extensions.searchableWithoutDiacritics
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import timber.log.Timber
import java.time.OffsetDateTime
import java.util.*

class FlightLogRepository(private val dao: FlightLogDao) {
    val activeScenario: LiveData<Scenario> = dao.activeScenario()
    val pilots = dao.getPilots()
    val lastStartedPilot: Flow<Map<Pilot, Flight>> = dao.lastStartedPilot()

    //val scenarios = dao.getScenarios()
    val scenariosWithAttributes = dao.getScenariosWithAttributes()

    //fun getFlights(pilotUID: UUID) = dao.getFlights(pilotUID)

    fun getFlightsAndScenarios(pilotUID: UUID) = dao.getFlightsAndScenarios(pilotUID)

    @WorkerThread
    suspend fun update(pilot: Pilot) = dao.update(pilot)

    @WorkerThread
    suspend fun getPilotsForExport(pilotsUuids: Set<UUID>): List<Pilot> {
        if (pilotsUuids.isEmpty()) {
            return dao.getPilotsForExport()
        }
        return dao.getPilotsForExport(pilotsUuids)
    }

    @WorkerThread
    suspend fun update(scenario: Scenario, attrs: List<GenericScenarioAttribute>) {
        dao.transactional {
            dao.update(scenario)
            attrs.forEach {
                dao.update(it)
            }
        }
    }

    @WorkerThread
    suspend fun update(scenario: Scenario) = dao.update(scenario)

    @WorkerThread
    fun pilots(searchText: String): Flow<List<Pilot>> {
        val filter = searchText.searchableWithoutDiacritics()
        return pilots.map { list ->
            list.filter { pilot ->
                val customerID = pilot.customerID
                val status = pilot.status
                val vehicle = pilot.currentFlightVehicle
                val fullname = pilot.firstname + " " + pilot.lastname
                fullname.searchableWithoutDiacritics()
                    .indexOf(filter, 0, true) >= 0
                        || (customerID != null && (customerID.searchableWithoutDiacritics()
                    .indexOf(filter, 0, true) >= 0))
                        || status.searchableWithoutDiacritics()
                    .indexOf(filter, 0, true) >= 0
                        || vehicle.searchableWithoutDiacritics()
                    .indexOf(filter, 0, true) >= 0
            }
        }
    }

    @WorkerThread
    fun scenariosWithAttributes(searchText: String): Flow<Map<Scenario, List<GenericScenarioAttribute>>> {
        val filter = searchText.searchableWithoutDiacritics()
        return scenariosWithAttributes.map { map ->
            map.filter { scenario ->
                scenario.key.name.searchableWithoutDiacritics()
                    .indexOf(filter, 0, true) >= 0
            }
        }
    }

    @WorkerThread
    suspend fun insert(pilot: Pilot): Result {
        var result = Result.OK
        dao.transactional {
            val customerID = pilot.customerID
            if (!customerID.isNullOrBlank()) {
                if (dao.getPilot(customerID) != null) {
                    result = Result.PILOT_WITH_CUSTOMER_ID_EXISTS
                    return@transactional
                }
            }
            dao.insert(pilot)
        }
        return result
    }

    enum class Result {
        OK,
        PILOT_WITH_CUSTOMER_ID_EXISTS,
        SCENARIO_WITH_NAME_EXISTS
    }

    @WorkerThread
    suspend fun insert(scenario: Scenario, attrs: List<GenericScenarioAttribute>): Result {
        Timber.d("insert scenario ${scenario.name} ${scenario.sUid}")
        var result = Result.OK
        dao.transactional {
            if (dao.getScenario(scenario.name) != null) {
                Timber.w("scenario with name [${scenario.name}] already exists")
                result = Result.SCENARIO_WITH_NAME_EXISTS
                return@transactional
            }
            dao.insert(scenario)
            attrs.forEach {
                dao.insert(GenericScenarioAttribute(scenario, it.key, it.value))
            }
        }
        return result
    }

    @WorkerThread
    fun getPilot(pilotUID: UUID) = dao.getPilot(pilotUID)

    @WorkerThread
    suspend fun insert(flight: Flight) = dao.insert(flight)

    @WorkerThread
    suspend fun launch(flight: Flight) {
        dao.transactional {
            flight.scenarioUid = dao.activeScenarioUid()
            insert(flight)
            setActive(setOf(flight.pilotUid), true)
        }
    }

    @WorkerThread
    suspend fun setActive(uuids: Set<UUID>, status: Boolean) =
        dao.setActive(uuids, status)

    @WorkerThread
    suspend fun setSingleScenarioActive(scenario: Scenario) =
        dao.setSingleScenarioActive(scenario.sUid)

    @WorkerThread
    suspend fun deletePilots(uuids: Set<UUID>) = dao.deletePilots(uuids)

    @WorkerThread
    suspend fun deleteFlights(uuids: Set<UUID>) = dao.deleteFlights(uuids)

    @WorkerThread
    suspend fun deleteScenarios(uuids: Set<UUID>) = dao.deleteScenarios(uuids)

    @WorkerThread
    suspend fun update(flight: Flight) = dao.update(flight)

    @WorkerThread
    suspend fun setActiveScenarioToFlights(uuids: Set<UUID>) {
        dao.setActiveScenarioToFlights(uuids)
    }

    @WorkerThread
    suspend fun getPilotsWithFlights(pilotUuids: Set<UUID>): Map<Pilot, List<Flight>> {
        return dao.getPilotsWithFlights(pilotUuids)
    }

    @WorkerThread
    suspend fun getAllPilotsWithFlights(): Map<Pilot, List<Flight>> {
        return dao.getAllPilotsWithFlights()
    }

    @WorkerThread
    suspend fun getPilotsWithFlights(
        pilotUuids: Set<UUID>,
        rangeStart: OffsetDateTime,
        rangeEnd: OffsetDateTime
    ): Map<Pilot, List<Flight>> {
        return dao.getPilotsWithFlights(pilotUuids, rangeStart, rangeEnd)
    }

    @WorkerThread
    suspend fun getAllPilotsWithFlights(
        rangeStart: OffsetDateTime,
        rangeEnd: OffsetDateTime
    ): Map<Pilot, List<Flight>> {
        return dao.getAllPilotsWithFlights(rangeStart, rangeEnd)
    }

    @WorkerThread
    suspend fun getScenarioWithAttributes(scenarioUid: UUID): Map<Scenario, List<GenericScenarioAttribute>> {
        return dao.getScenarioWithAttributes(scenarioUid)
    }

    companion object {
        private val emptyScenarioAttribute = GenericScenarioAttribute(
            UUID.randomUUID(),
            "",
            ""
        )
        val emptyScenarioWithAttributes = mapOf(
            (Scenario() to listOf(
                emptyScenarioAttribute,
                emptyScenarioAttribute,
                emptyScenarioAttribute,
            ))
        )
    }

    suspend fun importPilots(pilotCsv: List<Exporter.PilotCSV>) {
        dao.transactional {
            pilotCsv.forEach { csv ->
                if (!csv.customerID.isNullOrBlank()) {
                    val pilot = dao.getPilot(csv.customerID)
                    if (pilot != null) {
                        csv.updatePilot(pilot)
                        dao.update(pilot)
                    } else {
                        dao.insert(csv.createPilot())
                    }
                } else {
                    dao.insert(csv.createPilot())
                }
            }
        }
    }


}
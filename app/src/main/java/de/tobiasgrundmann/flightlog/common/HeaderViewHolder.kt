/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.common

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import de.tobiasgrundmann.flightlog.R

class HeaderViewHolder(itemView: View) :
    SwipeForActivationViewHolder(itemView, itemView, itemView, itemView, itemView) {
    private val headerText: TextView = itemView.findViewById(R.id.header_text)
    private val activeStr: String = itemView.context.getString(R.string.active)
    private val notActiveStr: String = itemView.context.getString(R.string.not_active)

    companion object {
        fun create(
            parent: ViewGroup
        ): HeaderViewHolder {
            val view: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.recyclerview_pilotlist_header, parent, false)

            return HeaderViewHolder(view)
        }
    }

    fun bind(active: Boolean) {
        headerText.isActivated = active
        if (active) {
            headerText.text = activeStr
        } else {
            headerText.text = notActiveStr
        }
    }
}
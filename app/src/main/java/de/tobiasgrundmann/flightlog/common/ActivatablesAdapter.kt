/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.common

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

abstract class ActivatablesAdapter<T : Activatable, VH : RecyclerView.ViewHolder>(
    recyclerview: RecyclerView,
) : SelectionListAdapter<Activatable, VH>(recyclerview, COMPARATOR) {
    protected val displayList: MutableList<Activatable> = mutableListOf()
    abstract fun allowSwipe(): Boolean
    abstract fun openEditDialog(activatable: Activatable)
    abstract fun toggleActivation(activatable: Activatable)
    public override fun getItem(position: Int): Activatable = super.getItem(position)

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<Activatable>() {
            override fun areItemsTheSame(oldItem: Activatable, newItem: Activatable): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(oldItem: Activatable, newItem: Activatable): Boolean {
                return oldItem.uuid() == newItem.uuid()
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is ActivatableHeader -> ItemType.HEADER
            else -> ItemType.ITEM
        }
    }

    fun submitListAddHeader(list: List<Activatable>) {
        displayList.clear()
        val actives = list.filter { it.activated() }
        val inactives = list.filter { !it.activated() }
        if (actives.isNotEmpty()) {
            displayList.add(ActivatableHeader.activeHeader)
            displayList.addAll(actives)
        }
        if (inactives.isNotEmpty()) {
            displayList.add(ActivatableHeader.inactiveHeader)
            displayList.addAll(inactives)
        }
//        displayList.forEach { d ->
//            Timber.d("submitList $d")
//        }
        submitSelectables(list.map { p -> p.uuid() })
        submitList(displayList.toList())
    }

}
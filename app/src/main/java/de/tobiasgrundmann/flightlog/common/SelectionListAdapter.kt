/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.common

import android.view.ActionMode
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import java.util.*

abstract class SelectionListAdapter<T, VH : RecyclerView.ViewHolder>(
    val recyclerview: RecyclerView,
    diffCallback: DiffUtil.ItemCallback<T>
) : ListAdapter<T, VH>(diffCallback) {

    private var actionMode: ActionMode? = null
    private val selection: MutableSet<UUID> = HashSet()
    private var selectables: MutableSet<UUID> = HashSet()

    override fun onViewRecycled(holder: VH) {
        super.onViewRecycled(holder)
        holder.itemView.isSelected = false
    }

    fun getSelection() = selection.toHashSet()
    fun getAndResetSelection(): Set<UUID> {
        val sel = getSelection()
        sel.forEach(::toggleSelection)
        assert(selection.isEmpty())
        return sel
    }

    /**
     * Selects all items if at least one is not selected
     * Unselects all if all are selected
     */
    fun selectAllOrNothing() {
        val func = if (selection.size < selectables.size) {
            //Timber.d("select all")
            selection::add
        } else {
            //Timber.d("select nothing")
            selection::remove
        }
        selectables.forEach {
            if (func(it)) {
                notifyItemChanged(getPosition(it))
            }
        }
    }

    private fun toggleSelection(uid: UUID): Boolean {
        val position = getPosition(uid)
        return if (uid in selection) {
            selection.remove(uid)
            false
        } else {
            selection.add(uid)
            true
        }.also {
            //Timber.d("notifyItemChanged($position)")
            notifyItemChanged(position)
        }
    }

    fun onLongClick(uuid: UUID): Boolean {
        val selected = toggleSelection(uuid)
        if (selection.isEmpty()) {
            actionMode?.finish()
            actionMode = null
        } else if (actionMode == null) {
            actionMode = recyclerview.startActionMode(getActionModeCallback())
            //Timber.d("Action Mode activated")
        }
        return selected
    }

    fun isSelected(uuid: UUID) = selection.contains(uuid)

    fun isActionMode() = actionMode != null

    fun destroyActionMode() {
        getAndResetSelection()
        actionMode = null
    }

    fun onClick(uuid: UUID): Boolean {
        if (isActionMode()) {
            return toggleSelection(uuid)
        }
        return false
    }

    fun submitSelectables(uids: Collection<UUID>) {
        selectables.clear()
        selectables.addAll(uids)
    }

    abstract fun getActionModeCallback(): ActionMode.Callback
    abstract fun getPosition(uid: UUID): Int
}
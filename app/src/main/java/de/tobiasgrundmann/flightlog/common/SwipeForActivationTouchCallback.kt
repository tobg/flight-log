/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.common

import android.graphics.Canvas
import android.view.View
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import timber.log.Timber

class SwipeForActivationTouchCallback<T : Activatable>(private val adapter: ActivatablesAdapter<T, SwipeForActivationViewHolder>) :
    ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun getSwipeDirs(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int {
        if (!adapter.allowSwipe()) {
            Timber.d("no swipe")
            return 0
        }
        return when (viewHolder.itemViewType) {
            ItemType.HEADER -> 0
            else -> super.getSwipeDirs(recyclerView, viewHolder)
        }
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val activatable = adapter.getItem(viewHolder.adapterPosition)
        when (viewHolder.itemViewType) {
            ItemType.ITEM -> {
                when (direction) {
                    ItemTouchHelper.LEFT -> {
                        adapter.openEditDialog(activatable)
                        adapter.notifyItemChanged(viewHolder.adapterPosition)
                    }
                    ItemTouchHelper.RIGHT -> {
                        adapter.toggleActivation(activatable)
                        adapter.notifyItemChanged(viewHolder.adapterPosition)
                    }
                }
            }
            else -> throw IllegalArgumentException("Can't swipe viewtype ${viewHolder.itemViewType}")
        }
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        viewHolder?.let {
            viewHolder as SwipeForActivationViewHolder
            getDefaultUIUtil().onSelected(viewHolder.clipForeground)
        }
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        viewHolder as SwipeForActivationViewHolder
        drawBackground(viewHolder, dX, actionState)
        getDefaultUIUtil().onDraw(
            c, recyclerView, viewHolder.clipForeground, dX, dY,
            actionState, isCurrentlyActive
        )
    }

    override fun onChildDrawOver(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder?,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        viewHolder as SwipeForActivationViewHolder
        drawBackground(viewHolder, dX, actionState)
        getDefaultUIUtil().onDrawOver(
            c, recyclerView, viewHolder.clipForeground, dX, dY,
            actionState, isCurrentlyActive
        )
    }

    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        viewHolder as SwipeForActivationViewHolder
        viewHolder.clipBackgroundActivate.visibility = View.GONE
        viewHolder.clipBackgroundDeactivate.visibility = View.GONE
        viewHolder.clipBackgroundEdit.visibility = View.GONE
        getDefaultUIUtil().clearView(viewHolder.clipForeground)
    }

    private fun drawBackground(
        viewHolder: SwipeForActivationViewHolder,
        dX: Float,
        actionState: Int
    ) {
        if (RecyclerView.NO_POSITION == viewHolder.adapterPosition) {
            return
        }
        val activatable = adapter.getItem(viewHolder.adapterPosition)
        val swipeRightBackground = if (activatable.activated()) {
            viewHolder.clipBackgroundDeactivate
        } else {
            viewHolder.clipBackgroundActivate
        }
        val swipeLeftBackground = viewHolder.clipBackgroundEdit

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            if (dX > 0) {
                swipeLeftBackground.visibility = View.GONE
                swipeRightBackground.visibility = View.VISIBLE
                swipeRightBackground.right = dX.toInt()
                swipeRightBackground.left = 0
            } else {
                swipeRightBackground.visibility = View.GONE
                swipeLeftBackground.visibility = View.VISIBLE
                swipeLeftBackground.right = viewHolder.clipForeground.width
                swipeLeftBackground.left = viewHolder.clipForeground.right + dX.toInt()
            }
        }
    }
}
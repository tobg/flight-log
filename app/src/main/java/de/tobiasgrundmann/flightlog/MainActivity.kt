/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.MutableLiveData
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.google.android.material.snackbar.Snackbar
import de.tobiasgrundmann.flightlog.database.Exporter
import de.tobiasgrundmann.flightlog.databinding.ActivityMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.OffsetDateTime
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    lateinit var binding: ActivityMainBinding

    private val exportURI = MutableLiveData<Uri>()
    val userMessage = MutableLiveData(MessageToUser.noMessage)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        exportURI.observe(this) {
            val shareIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_STREAM, it)
                putExtra(Intent.EXTRA_SUBJECT, getString(R.string.flights_export_subject))
                //putExtra(Intent.EXTRA_TEXT, "")
                type = "text/csv"
                flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            }
            startActivity(Intent.createChooser(shareIntent, "SHARE"))
        }

        userMessage.observe(this) {
            if (it.hasMessage()) {
                Snackbar.make(
                    binding.root,
                    it.getString(resources),
                    Snackbar.LENGTH_INDEFINITE
                ).show()
                userMessage.value = MessageToUser.noMessage
            }
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment_content_main) as NavHostFragment
        val navController = navHostFragment.navController
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.options_menu, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu.findItem(R.id.menu_search).actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            isSubmitButtonEnabled = false
        }
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }

    private val getPilotsLocal =
        registerForActivityResult(ActivityResultContracts.OpenDocument()) { uri ->
            if (uri != null) {
                val fApplication = application as FlightLogApplication
                fApplication.applicationScope.launch {
                    Exporter(fApplication).importPilots(uri, userMessage)
                }
            }
        }

    fun importPilotsLocal() {
        getPilotsLocal.launch(
            arrayOf(
                "text/csv", "text/comma-separated-values", "application/csv"
            )
        )
    }

    fun exportFlights(pilotsUuids: Set<UUID>, dateRange: Pair<OffsetDateTime?, OffsetDateTime?>) {
        val fApplication = application as FlightLogApplication
        fApplication.applicationScope.launch {
            Exporter(fApplication).exportFlights(exportURI, pilotsUuids, dateRange)
        }
    }

    fun exportPilots(pilotsUuids: Set<UUID>) {
        val fApplication = application as FlightLogApplication
        fApplication.applicationScope.launch {
            Exporter(fApplication).exportPilots(exportURI, pilotsUuids)
        }
    }

    fun importPilotsFromServer() {
        val fApplication = application as FlightLogApplication
        fApplication.applicationScope.launch(Dispatchers.IO) {
            Exporter(fApplication).importPilotsFromServer(userMessage)
        }

    }
}
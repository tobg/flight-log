/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.scenarios

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import de.tobiasgrundmann.flightlog.FlightLogApplication
import de.tobiasgrundmann.flightlog.MainActivity
import de.tobiasgrundmann.flightlog.R
import de.tobiasgrundmann.flightlog.common.SwipeForActivationTouchCallback
import de.tobiasgrundmann.flightlog.databinding.FragmentScenariolistBinding
import de.tobiasgrundmann.flightlog.extensions.resetBackStackHandler

class ScenariosFragment : Fragment() {
    private var scenariosAdapter: ScenariosAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        //Timber.d("onCreateView")

        val binding: FragmentScenariolistBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_scenariolist, container, false
        )
        val mainActivity = requireActivity() as MainActivity
        val application = mainActivity.application as FlightLogApplication
        resetBackStackHandler()
        val viewModelFactory =
            ScenariosViewModelFactory(
                application.repository,
                application.applicationScope,
                mainActivity.userMessage
            )
        val viewModel =
            ViewModelProvider(this, viewModelFactory).get(ScenariosViewModel::class.java)
        val adapter = ScenariosAdapter(viewModel, binding.recyclerviewScenariolist)
        val touchHelper = ItemTouchHelper(SwipeForActivationTouchCallback(adapter))
        touchHelper.attachToRecyclerView(binding.recyclerviewScenariolist)

        viewModel.scenarios.observe(viewLifecycleOwner) {
            //Timber.d("viewModel.scenarios.observe ${it}")
            adapter.submitListAddHeader(it)
        }
        viewModel.openScenarioEditDialog.observe(viewLifecycleOwner) {
            if (it) {
                val dialogFragment = ScenarioEditDialog(
                    viewModel.scenario,
                    viewModel.scenarioAttributes
                ) {
                    viewModel.saveScenario()
                }
                dialogFragment.show(parentFragmentManager, "EDIT_SCENARIO_DIALOG")
                viewModel.scenarioEditDialogDone()
            }
        }
        mainActivity.supportActionBar?.title = getString(R.string.scenarios)

        setHasOptionsMenu(true)

        scenariosAdapter = adapter
        binding.scenariosViewModel = viewModel
        binding.recyclerviewScenariolist.adapter = adapter
        binding.recyclerviewScenariolist.layoutManager = LinearLayoutManager(this.requireContext())
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root

    }

//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        return NavigationUI.onNavDestinationSelected(item, requireView().findNavController())
//                || super.onOptionsItemSelected(item)
//    }
//    override fun onPrepareOptionsMenu(menu: Menu) {
//        super.onPrepareOptionsMenu(menu)
//        val searchView = menu.findItem(R.id.menu_search).actionView as SearchView
//        searchView.setOnQueryTextListener(scenariosAdapter)
//        searchView.setOnCloseListener(scenariosAdapter)
//    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.clear()
        //menu.removeItem(R.id.menu_search)
        super.onPrepareOptionsMenu(menu)
    }

}
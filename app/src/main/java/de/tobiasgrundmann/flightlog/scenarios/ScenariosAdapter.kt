/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.scenarios

import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import de.tobiasgrundmann.flightlog.R
import de.tobiasgrundmann.flightlog.common.*
import de.tobiasgrundmann.flightlog.database.GenericScenarioAttribute
import de.tobiasgrundmann.flightlog.database.Scenario
import timber.log.Timber
import java.util.*
import kotlin.collections.HashMap

class ScenariosAdapter(
    val viewModel: ScenariosViewModel,
    recyclerview: RecyclerView
) : ActivatablesAdapter<Scenario, SwipeForActivationViewHolder>(
    recyclerview,
),
    SearchView.OnQueryTextListener,
    SearchView.OnCloseListener {

    private var scenarioMap: MutableMap<Scenario, List<GenericScenarioAttribute>> = HashMap()
    var searchText: String = ""

    private val myActionModeCallback = object : ActionMode.Callback {
        override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
            val inflater: MenuInflater = mode.menuInflater
            inflater.inflate(R.menu.cab_menu_scenarios, menu)
            return true
        }

        override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
            Timber.d("onPrepareActionMode")
            return false
        }

        override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
            return when (item.itemId) {
                R.id.menu_select_all -> {
                    selectAllOrNothing()
                    true
                }
                R.id.menu_delete -> {
                    val selectedUuids = getSelection()
                    MaterialAlertDialogBuilder(recyclerview.context)
                        .setTitle(recyclerview.context.getString(R.string.delete_scenarios_title))
                        .setMessage(
                            recyclerview.context.resources.getQuantityString(
                                R.plurals.delete_scenarios_msg,
                                selectedUuids.size,
                                selectedUuids.size
                            )
                        )
                        .setPositiveButton(
                            R.string.ok
                        ) { _, _ ->
                            getAndResetSelection()
                            viewModel.deleteScenarios(selectedUuids)
                            mode.finish()
                        }
                        .setNegativeButton(
                            R.string.cancel
                        ) { dialog, _ ->
                            dialog.cancel()
                        }
                        .show()
                    true
                }
                else -> false
            }
        }

        override fun onDestroyActionMode(mode: ActionMode) {
            destroyActionMode()
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SwipeForActivationViewHolder {
        return when (viewType) {
            ItemType.HEADER -> HeaderViewHolder.create(parent)
            ItemType.ITEM -> ScenarioViewHolder.create(parent)
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun onViewRecycled(holder: SwipeForActivationViewHolder) {
        super.onViewRecycled(holder)
        holder.itemView.isActivated = false
    }

    override fun onBindViewHolder(holder: SwipeForActivationViewHolder, position: Int) {
        when (holder) {
            is HeaderViewHolder -> {
                val thing = getItem(position) as ActivatableHeader
                holder.bind(thing.active)
            }
            is ScenarioViewHolder -> {
                val thing = getItem(position)
                holder.bind(thing as Scenario, this)
            }
        }
    }

    private fun updateSearch(searchText: String) {
        this.searchText = searchText
        viewModel.setSearchText(searchText)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        updateSearch(newText ?: "")
        return true
    }

    override fun onClose(): Boolean {
        updateSearch("")
        return true
    }

    override fun getPosition(uid: UUID) = displayList.indexOfFirst { it.uuid() == uid }

    override fun getActionModeCallback(): ActionMode.Callback {
        return myActionModeCallback
    }

    override fun openEditDialog(activatable: Activatable) {
        activatable as Scenario
        viewModel.scenario = activatable
        viewModel.scenarioAttributes.clear()
        viewModel.scenarioAttributes.addAll(scenarioMap.get(activatable)!!)
        viewModel.openScenarioEditDialog(new = false)
    }

    override fun allowSwipe(): Boolean = !isActionMode()

    override fun toggleActivation(activatable: Activatable) {
        activatable as Scenario

        if (activatable.toggleActivation()) {
            viewModel.setSingleScenarioActive(activatable)
        } else {
            viewModel.update(activatable)
        }
    }

    fun submitListAddHeader(map: Map<Scenario, List<GenericScenarioAttribute>>?) {
        if (map != null) {
            this.scenarioMap.clear()
            this.scenarioMap.putAll(map)
            super.submitListAddHeader(map.keys.toList())
        }

    }


}

/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.scenarios

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import de.tobiasgrundmann.flightlog.MessageId
import de.tobiasgrundmann.flightlog.MessageToUser
import de.tobiasgrundmann.flightlog.database.FlightLogRepository
import de.tobiasgrundmann.flightlog.database.GenericScenarioAttribute
import de.tobiasgrundmann.flightlog.database.Scenario
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.launch
import java.util.*

class ScenariosViewModel(
    private val repository: FlightLogRepository,
    private val applicationScope: CoroutineScope,
    private val userMessage: MutableLiveData<MessageToUser>
) : ViewModel() {

    private val searchText = MutableStateFlow("")

    private val _openScenarioEditDialog = MutableLiveData(false)
    val openScenarioEditDialog: LiveData<Boolean>
        get() = _openScenarioEditDialog

    @OptIn(ExperimentalCoroutinesApi::class)
    val scenarios: LiveData<Map<Scenario, List<GenericScenarioAttribute>>> =
        searchText.flatMapLatest { sText ->
            //Timber.d("search text [$sText]")
            if ("" == sText) {
                repository.scenariosWithAttributes
            } else {
                repository.scenariosWithAttributes(sText)
            }
        }.asLiveData()

    var scenario = Scenario()
    val scenarioAttributes: MutableList<GenericScenarioAttribute> = ArrayList()
    private var newScenario: Boolean = false

    fun setSingleScenarioActive(scenario: Scenario) = applicationScope.launch {
        repository.setSingleScenarioActive(scenario)
    }

    fun update(scenario: Scenario) = applicationScope.launch {
        repository.update(scenario)
    }

    fun saveScenario() {
        //Timber.d("saveScenario $scenario new:$newScenario")
        if (scenario.name.isBlank()) {
            return
        }
        applicationScope.launch {
            if (newScenario) {
                if (repository.insert(scenario, scenarioAttributes)
                    == FlightLogRepository.Result.SCENARIO_WITH_NAME_EXISTS
                ) {
                    userMessage.postValue(MessageToUser(
                        MessageId.SCENARIO_WITH_NAME_EXISTS
                    ))
                }
            } else {
                repository.update(scenario, scenarioAttributes)
            }
            newScenario = false
        }
    }

    fun setSearchText(text: String) {
        searchText.value = text
    }

    fun openScenarioEditDialog(new: Boolean) {
        newScenario = new
        if (new) {
            this.scenario = Scenario(searchText.value)
            this.scenarioAttributes.clear()
            this.scenarioAttributes.addAll(
                listOf(
                    GenericScenarioAttribute(scenario, "attr0", ""),
                    GenericScenarioAttribute(scenario, "attr1", ""),
                    GenericScenarioAttribute(scenario, "attr2", "")
                )
            )
        }
        _openScenarioEditDialog.value = true
    }

    fun scenarioEditDialogDone() {
        _openScenarioEditDialog.value = false
    }

    fun deleteScenarios(uuids: Set<UUID>) {
        //Timber.d("deleteScenarios $uuids")
        applicationScope.launch {
            repository.deleteScenarios(uuids)
        }
    }
}
/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.scenarios

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import de.tobiasgrundmann.flightlog.MessageToUser
import de.tobiasgrundmann.flightlog.database.FlightLogRepository
import kotlinx.coroutines.CoroutineScope

class ScenariosViewModelFactory(
    private val repository: FlightLogRepository,
    private val applicationScope: CoroutineScope,
    private val userMessage: MutableLiveData<MessageToUser>
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ScenariosViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ScenariosViewModel(repository, applicationScope, userMessage) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
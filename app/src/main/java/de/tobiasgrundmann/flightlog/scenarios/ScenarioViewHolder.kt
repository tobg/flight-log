/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.scenarios

import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import de.tobiasgrundmann.flightlog.R
import de.tobiasgrundmann.flightlog.common.SwipeForActivationViewHolder
import de.tobiasgrundmann.flightlog.database.Scenario
import de.tobiasgrundmann.flightlog.extensions.highlightText

class ScenarioViewHolder(itemView: View) : SwipeForActivationViewHolder(
    itemView,
    clipForeground = itemView.findViewById(R.id.clipForeground),
    clipBackgroundActivate = itemView.findViewById(R.id.clipBackgroundActivate),
    clipBackgroundDeactivate = itemView.findViewById(R.id.clipBackgroundDeactivate),
    clipBackgroundEdit = itemView.findViewById(R.id.clipBackgroundEdit)
) {
    var scenario: Scenario? = null
    private val scenarioButton: Button = itemView.findViewById(R.id.pilotlist_pilot)

    companion object {
        fun create(
            parent: ViewGroup
        ): ScenarioViewHolder {
            val view: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.recyclerview_pilotlist_item, parent, false)

            return ScenarioViewHolder(view)
        }
    }

    fun bind(scenario: Scenario, adapter: ScenariosAdapter) {
        this.scenario = scenario
        clipForeground.isActivated = scenario.active
        clipForeground.isSelected = adapter.isSelected(scenario.sUid)
        scenarioButton.setOnClickListener {
            if (adapter.isActionMode()) {
                adapter.onClick(scenario.sUid)
            } else {
                adapter.openEditDialog(scenario)
                adapter.notifyItemChanged(adapterPosition)
            }
        }
        scenarioButton.setOnLongClickListener {
            //Timber.d("Long Click Button")
            adapter.onLongClick(scenario.sUid)
            true
        }
        val ssb = SpannableStringBuilder()
        with(ssb) {
            append(scenario.name.highlightText(adapter.searchText))
        }.also { scenarioButton.text = it }
    }


}
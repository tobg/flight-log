/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.scenarios

import android.app.Dialog
import android.os.Bundle
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import de.tobiasgrundmann.flightlog.MainActivity
import de.tobiasgrundmann.flightlog.R
import de.tobiasgrundmann.flightlog.database.GenericScenarioAttribute
import de.tobiasgrundmann.flightlog.database.Scenario

class ScenarioEditDialog(
    val scenario: Scenario,
    private val attributes: List<GenericScenarioAttribute>,
    val onOK: () -> Unit
) : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        assert(attributes.size == 3)
        val mainActivity = requireActivity() as MainActivity
        return mainActivity.let {
            val builder = MaterialAlertDialogBuilder(it)

            val inflater = it.layoutInflater
            val view = inflater.inflate(R.layout.dialog_edit_scenario, null)
            val name: TextView = view.findViewById(R.id.textinput_scenario_text)
            val attr0: TextView = view.findViewById(R.id.textinput_scenario_attr0_text)
            val attr1: TextView = view.findViewById(R.id.textinput_scenario_attr1_text)
            val attr2: TextView = view.findViewById(R.id.textinput_scenario_attr2_text)

            name.text = scenario.name
            attr0.text = attributes[0].value
            attr1.text = attributes[1].value
            attr2.text = attributes[2].value

            builder.setView(view)
                .setPositiveButton(
                    R.string.ok
                ) { _, _ ->
                    scenario.name = name.text.toString()
                    attributes[0].value = attr0.text.toString()
                    attributes[1].value = attr1.text.toString()
                    attributes[2].value = attr2.text.toString()
                    onOK()
                }
                .setNegativeButton(
                    R.string.cancel
                ) { dialog, _ ->
                    dialog.cancel()
                }
            builder.create()
        }
    }
}
/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.preferences

import android.content.res.Resources
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import de.tobiasgrundmann.flightlog.FlightLogApplication
import de.tobiasgrundmann.flightlog.R
import de.tobiasgrundmann.flightlog.databinding.FragmentSettingsBinding
import de.tobiasgrundmann.flightlog.extensions.resetBackStackHandler
import timber.log.Timber
import java.net.MalformedURLException
import java.net.URL
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets

class SettingsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: FragmentSettingsBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_settings, container, false
        )
        val application = requireActivity().application as FlightLogApplication
        resetBackStackHandler()

        val viewModelFactory =
            SettingsViewModelFactory(application.preferencesRepository)
        val viewModel =
            ViewModelProvider(this, viewModelFactory).get(SettingsViewModel::class.java)
        val spinner: Spinner = binding.charsetSpinner
        val characterSets = mutableListOf(
            StandardCharsets.ISO_8859_1.name(),
            StandardCharsets.UTF_8.name(),
            StandardCharsets.US_ASCII.name(),
            StandardCharsets.UTF_16.name(),
            StandardCharsets.UTF_16BE.name(),
            StandardCharsets.UTF_16LE.name(),
        )
        characterSets.addAll(Charset.availableCharsets().keys.filter {
            it.contains(
                "windows",
                true
            )
        }
            .toList())
        val adapter = ArrayAdapter(
            requireContext(), android.R.layout.simple_spinner_item,
            characterSets
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val charsetName = adapter.getItem(position)
                viewModel.updateImportExportCharset(Charset.forName(charsetName))
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
        spinner.adapter = adapter

        val pilotImportUrlText: TextView = binding.textinputImportPilotsUrlText
        val textWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (!s.isNullOrEmpty() && s.isNotBlank()) {
                    try {
                        val url = URL(s.toString().trim())
                        pilotImportUrlText.setTextColor(textColor(android.R.attr.textColorPrimary))
                        viewModel.updatePilotImportUrl(url)
                    } catch (e: MalformedURLException) {
                        Timber.e(message = "malformed url [$s]", t = e)
                        pilotImportUrlText.setTextColor(textColor(android.R.attr.colorError))
                    }
                } else {
                    viewModel.updatePilotImportUrl(null)
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }
        }
        pilotImportUrlText.addTextChangedListener(textWatcher)

        viewModel.userPreferences.observe(viewLifecycleOwner) {
            val pos = characterSets.indexOf(it.importExportCharset.name())
            spinner.setSelection(pos)
            val url = it.pilotImportUrl?.toString() ?: ""
            if (pilotImportUrlText.text.toString() != url) {
                pilotImportUrlText.text = url
            }
            viewModel.userPreferences.removeObservers(viewLifecycleOwner)
        }

        binding.settingsViewModel = viewModel
        setHasOptionsMenu(true)
        return binding.root
    }

    private fun textColor(id: Int): Int {
        val typedValue = TypedValue()
        val theme: Resources.Theme = requireContext().theme
        val resolved = theme.resolveAttribute(
            id,
            typedValue,
            true
        )
        return if (resolved) {
            val colorRes = typedValue.run { if (resourceId != 0) resourceId else data }
            ContextCompat.getColor(requireContext(), colorRes)
        } else {
            R.color.primaryTextColor
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.clear()
        super.onPrepareOptionsMenu(menu)
    }

}
/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog

import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.Build
import android.widget.Toast
import androidx.datastore.preferences.preferencesDataStore
import com.google.android.material.color.DynamicColors
import de.tobiasgrundmann.flightlog.database.FlightLogDatabase
import de.tobiasgrundmann.flightlog.database.FlightLogRepository
import de.tobiasgrundmann.flightlog.database.PreferencesRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import timber.log.Timber
import java.io.PrintWriter
import java.io.StringWriter
import kotlin.system.exitProcess

private const val USER_PREFERENCES_NAME = "user_preferences"
private val Context.dataStore by preferencesDataStore(
    name = USER_PREFERENCES_NAME
)

class FlightLogApplication : Application() {
    val applicationScope = CoroutineScope(SupervisorJob())
    private val database by lazy { FlightLogDatabase.getDatabase(this, applicationScope) }
    val repository by lazy { FlightLogRepository(database.dao()) }
    lateinit var preferencesRepository: PreferencesRepository

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        DynamicColors.applyToActivitiesIfAvailable(this)
        preferencesRepository = PreferencesRepository(this.dataStore)
        val oldHandler = Thread.getDefaultUncaughtExceptionHandler()
        Thread.setDefaultUncaughtExceptionHandler { t, e ->
            var exception = e
            try {
                val sw = StringWriter()
                sw.append("App: ${BuildConfig.APPLICATION_ID} Version: ${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE})\n")
                sw.append("Android: ${Build.VERSION.RELEASE} SDK: ${Build.VERSION.SDK_INT}\n")
                sw.append("Brand: ${Build.BRAND}\n")
                sw.append("Product: ${Build.PRODUCT}\n")
                sw.append("Model: ${Build.MODEL}\n")
                sw.append("Device: ${Build.DEVICE}\n")
                //sw.append("Display: ${Build.DISPLAY}\n")
                sw.append("Stacktrace:\n")
                e.printStackTrace(PrintWriter(sw))
                val shareIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_EMAIL, arrayOf("android-apps@tobias-grundmann.de"))
                    putExtra(Intent.EXTRA_SUBJECT, "Error report on Flight Log")
                    putExtra(Intent.EXTRA_TEXT, sw.toString())
                    type = "text/plain"
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK
                }
                try {
                    Toast.makeText(
                        this,
                        "Flight Log crashed. Please report an error. ${e.message}",
                        Toast.LENGTH_LONG
                    ).show()
                } catch (e: Exception) {
                    //ignore UI might be dead already
                }
                startActivity(shareIntent)
            } catch (e1: Exception) {
                e1.addSuppressed(e)
                exception = e1
            } finally {
                if (oldHandler != null) {
                    oldHandler.uncaughtException(t, exception)
                } else {
                    Timber.e(exception)
                    exitProcess(1)
                }
            }
        }
    }
}
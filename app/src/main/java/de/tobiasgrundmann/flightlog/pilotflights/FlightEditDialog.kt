/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.pilotflights

import android.app.Dialog
import android.os.Bundle
import android.text.format.DateFormat.is24HourFormat
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointBackward
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import de.tobiasgrundmann.flightlog.MainActivity
import de.tobiasgrundmann.flightlog.R
import de.tobiasgrundmann.flightlog.database.Flight
import java.time.Instant
import java.time.OffsetDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter.ofLocalizedDate
import java.time.format.DateTimeFormatter.ofLocalizedTime
import java.time.format.FormatStyle

class FlightEditDialog(val flight: Flight, private val onOK: Runnable) :
    DialogFragment() {
    companion object {
        private val dateMedium = ofLocalizedDate(FormatStyle.MEDIUM)
        private val timeShort = ofLocalizedTime(FormatStyle.SHORT)

    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val mainActivity = requireNotNull(this.activity) as MainActivity
        return mainActivity.let {
            val builder = MaterialAlertDialogBuilder(it)

            val inflater = requireActivity().layoutInflater
            val view = inflater.inflate(R.layout.dialog_edit_flight, null)
            val startDateBt: Button = view.findViewById(R.id.start_date)
            val startTimeBt: Button = view.findViewById(R.id.start_time)
            //val landingDate: TextView = view.findViewById(R.id.landing_date)
            val landingTimeBt: Button = view.findViewById(R.id.landing_time)
            val statusTv: TextView = view.findViewById(R.id.textinput_flight_status_text)
            val vehicleTv: TextView = view.findViewById(R.id.textinput_flight_vehicle_text)
            val landingConfirmedCheckBox: CheckBox =
                view.findViewById(R.id.checkBox_landing_confirmed)

            val isSystem24Hour = is24HourFormat(requireContext())
            val clockFormat = if (isSystem24Hour) TimeFormat.CLOCK_24H else TimeFormat.CLOCK_12H

            with(flight) {
                statusTv.text = status
                vehicleTv.text = vehicle
                landingConfirmedCheckBox.isChecked = landingConfirmed
                startDateBt.text = startTime.toLocalDate().format(dateMedium)
                startTimeBt.text = startTime.toLocalTime().format(timeShort)

                val calendarConstraints = CalendarConstraints.Builder()
                    .setValidator(DateValidatorPointBackward.now())
                    .build()

                val startDatePicker =
                    MaterialDatePicker.Builder
                        .datePicker()
                        .setCalendarConstraints(calendarConstraints)
                        .setTitleText(requireContext().getString(R.string.start_date))
                        .setSelection(startTime.toEpochSecond() * 1000)
                        .build()
                startDatePicker.addOnPositiveButtonClickListener { inst ->
                    val instant = Instant.ofEpochMilli(inst)
                    startTime = OffsetDateTime.ofInstant(instant, ZoneId.from(startTime))
                    startDateBt.text = startTime.toLocalDate().format(dateMedium)
                }
                startDateBt.setOnClickListener {
                    startDatePicker.show(parentFragmentManager, "START_DATE_PICKER")
                }

                val startTimePicker =
                    MaterialTimePicker.Builder()
                        .setTimeFormat(clockFormat)
                        .setHour(startTime.toLocalTime().hour)
                        .setMinute(startTime.toLocalTime().minute)
                        .build()
                startTimePicker.addOnPositiveButtonClickListener {
                    startTime = startTime.withHour(startTimePicker.hour)
                        .withMinute(startTimePicker.minute)

                    startTimeBt.text =
                        startTime.toLocalTime()?.format(timeShort)
                }
                startTimeBt.setOnClickListener {
                    startTimePicker.show(parentFragmentManager, "LANDING_TIME_PICKER")
                }

                val nowLocal = OffsetDateTime.now().toLocalDateTime()
                var landingHour = nowLocal.hour
                var landingMinute = nowLocal.minute
                if (landingTime != null) {
                    landingTimeBt.text = landingTime?.toLocalTime()?.format(timeShort)
                    landingHour = landingTime?.toLocalTime()?.hour ?: 0
                    landingMinute = landingTime?.toLocalTime()?.minute ?: 0
                } else {
                    landingTimeBt.text = "--"
                }

                val landingTimePicker =
                    MaterialTimePicker.Builder()
                        .setTimeFormat(clockFormat)
                        .setHour(landingHour)
                        .setMinute(landingMinute)
                        .build()
                landingTimePicker.addOnPositiveButtonClickListener {
                    landingTime = startTime.withHour(landingTimePicker.hour)
                        .withMinute(landingTimePicker.minute)

                    landingTimeBt.text =
                        landingTime?.toLocalTime()?.format(timeShort) ?: "Error"

                    landingConfirmedCheckBox.isChecked = true
                }
                landingTimeBt.setOnClickListener {
                    landingTimePicker.show(parentFragmentManager, "LANDING_TIME_PICKER")
                }
                landingConfirmedCheckBox.setOnCheckedChangeListener { _, isChecked ->
                    landingConfirmed = isChecked
                    if (!isChecked) {
                        landingTimeBt.text = "--"
                        landingTime = null
                    }
                }
            }

            builder
                .setView(view)
                .setPositiveButton(
                    R.string.save
                ) { _, _ ->
                    flight.status = statusTv.text.toString()
                    flight.vehicle = vehicleTv.text.toString()
                    onOK.run()
                }

                .setNegativeButton(
                    R.string.cancel
                ) { dialog, _ ->
                    dialog.cancel()
                }
            builder.create()
        }
    }
}
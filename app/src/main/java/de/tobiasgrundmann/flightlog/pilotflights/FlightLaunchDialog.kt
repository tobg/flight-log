/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.pilotflights

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import de.tobiasgrundmann.flightlog.MainActivity
import de.tobiasgrundmann.flightlog.R
import de.tobiasgrundmann.flightlog.database.Pilot

class FlightLaunchDialog(val pilot: Pilot, private val onOK: DialogInterface.OnClickListener) :
    DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val mainActivity = requireNotNull(this.activity) as MainActivity
        return mainActivity.let {
            val builder = MaterialAlertDialogBuilder(it)
            val inflater = requireActivity().layoutInflater
            val view = inflater.inflate(R.layout.dialog_launch_flight, null)
            val label: TextView = view.findViewById(R.id.label_launch)
            val vehicle: TextView = view.findViewById(R.id.label_launch_vehicle)

            val status = if (pilot.status.isNotBlank()) {
                "(${pilot.status})"
            } else {
                ""
            }
            label.text = requireContext().getString(R.string.launch_dialog, pilot.firstname, status)
            vehicle.text = pilot.currentFlightVehicle

            builder.setView(view)
                //.setIcon(AppCompatResources.getDrawable(requireContext(),(R.drawable.ic_logo)))
                .setPositiveButton(
                    R.string.ok, onOK
                )
                .setNegativeButton(
                    R.string.cancel
                ) { dialog, _ ->
                    dialog.cancel()
                }
            builder.create()
        }
    }
}
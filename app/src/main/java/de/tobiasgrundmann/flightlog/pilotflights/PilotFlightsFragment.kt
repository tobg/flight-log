/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.pilotflights

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import de.tobiasgrundmann.flightlog.MainActivity
import de.tobiasgrundmann.flightlog.R
import de.tobiasgrundmann.flightlog.databinding.FragmentPilotFlightsBinding
import de.tobiasgrundmann.flightlog.pilotlist.PilotlistViewModel

class PilotFlightsFragment : Fragment() {

    private var flightsAdapter: FlightlistAdapter? = null

    val viewModel: PilotlistViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: FragmentPilotFlightsBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_pilot_flights, container, false
        )
        val adapter = FlightlistAdapter(viewModel.detailViewModel, binding.recyclerviewFlightlist)

        viewModel.detailViewModel.flights.observe(viewLifecycleOwner) {
            adapter.submitFlights(it)
        }
        viewModel.detailViewModel.activeScenario.observe(viewLifecycleOwner) {
            val mainActivity = requireActivity() as MainActivity
            if (it != null) {
                mainActivity.supportActionBar?.title = it.name
            }
        }

        viewModel.detailViewModel.openFlightLaunchDialog.observe(viewLifecycleOwner) {
            if (it) {
                val pilot = viewModel.detailViewModel.pilotFlow.value
                if (pilot != null) {
                    val dialogFragment =
                        FlightLaunchDialog(pilot) { _, _ ->
                            viewModel.detailViewModel.launchFlight()
                        }
                    dialogFragment.show(parentFragmentManager, "FLIGHT_LAUNCH_DIALOG")
                }
                viewModel.detailViewModel.flightLaunchDialogDone()
            }
        }

        viewModel.detailViewModel.openFlightEditDialog.observe(viewLifecycleOwner) {
            if (it) {
                val dialogFragment = FlightEditDialog(viewModel.detailViewModel.flight!!) {
                    viewModel.detailViewModel.saveFlight()
                }
                dialogFragment.show(parentFragmentManager, "FLIGHT_EDIT_DIALOG")
                viewModel.detailViewModel.flightEditDialogDone()
            }
        }

        flightsAdapter = adapter
        binding.pilotFlightsViewModel = viewModel.detailViewModel
        binding.recyclerviewFlightlist.adapter = adapter
        binding.recyclerviewFlightlist.layoutManager = LinearLayoutManager(this.requireContext())
        binding.lifecycleOwner = viewLifecycleOwner

        setHasOptionsMenu(true)
        return binding.root
    }

}
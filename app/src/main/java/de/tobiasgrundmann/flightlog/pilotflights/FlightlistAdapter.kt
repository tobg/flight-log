/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.pilotflights

import android.view.*
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import de.tobiasgrundmann.flightlog.R
import de.tobiasgrundmann.flightlog.common.SelectionListAdapter
import de.tobiasgrundmann.flightlog.database.FlightAndScenario
import timber.log.Timber
import java.util.*

class FlightlistAdapter(
    val viewModel: PilotFlightsViewModel,
    recyclerview: RecyclerView
) : SelectionListAdapter<FlightAndScenario, RecyclerView.ViewHolder>(recyclerview, FLIGHT_COMPARATOR) {
    private var flightUIDs : MutableList<UUID> = mutableListOf()
    private val myActionModeCallback = object : ActionMode.Callback {
        override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
            val inflater: MenuInflater = mode.menuInflater
            inflater.inflate(R.menu.cab_menu_pilotflights, menu)
            return true
        }

        override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
            Timber.d("onPrepareActionMode")
            return false
        }

        override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
            return when (item.itemId) {
                R.id.menu_select_all -> {
                    selectAllOrNothing()
                    true
                }
                R.id.menu_to_active_scenario -> {
                    viewModel.setActiveScenarioToFlights(getAndResetSelection())
                    true
                }
                R.id.menu_delete -> {
                    val selectedUuids = getSelection()
                    MaterialAlertDialogBuilder(recyclerview.context)
                        .setTitle(recyclerview.context.getString(R.string.delete_flights_title))
                        .setMessage(
                            recyclerview.context.resources.getQuantityString(
                                R.plurals.delete_flights_msg,
                                selectedUuids.size,
                                selectedUuids.size
                            )
                        )
                        .setPositiveButton(
                            R.string.ok
                        ) { _, _ ->
                            getAndResetSelection()
                            viewModel.deleteFlights(selectedUuids)
                            mode.finish()

                        }
                        .setNegativeButton(
                            R.string.cancel
                        ) { dialog, _ ->
                            dialog.cancel()
                        }
                        .show()

                    true
                }
                else -> false
            }
        }

        override fun onDestroyActionMode(mode: ActionMode) {
            destroyActionMode()
        }
    }

    companion object {
        private val FLIGHT_COMPARATOR = object : DiffUtil.ItemCallback<FlightAndScenario>() {
            override fun areItemsTheSame(oldItem: FlightAndScenario, newItem: FlightAndScenario): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(oldItem: FlightAndScenario, newItem: FlightAndScenario): Boolean {
                return oldItem.flight.fUid == newItem.flight.fUid
            }
        }
    }

//    public override fun getItem(position: Int): Flight {
//        return super.getItem(position)
//    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return FlightViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as FlightViewHolder
        holder.bind(getItem(position) as FlightAndScenario,this)
    }

    override fun getActionModeCallback(): ActionMode.Callback {
        return myActionModeCallback
    }

    override fun getPosition(uid: UUID) = flightUIDs.indexOfFirst { it == uid }

    fun submitFlights(list: List<FlightAndScenario>?) {
        flightUIDs.clear()
        list?.let {
            flightUIDs.addAll(it.map { fs -> fs.flight.fUid })
        }
        submitSelectables(flightUIDs)
        super.submitList(list)
    }
}

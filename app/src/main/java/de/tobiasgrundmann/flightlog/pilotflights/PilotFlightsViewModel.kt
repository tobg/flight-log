/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.pilotflights

import androidx.lifecycle.*
import de.tobiasgrundmann.flightlog.database.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.launch
import java.time.OffsetDateTime
import java.util.*

class PilotFlightsViewModel(
    private val repository: FlightLogRepository,
    private val applicationScope: CoroutineScope,
) : ViewModel() {
    val pilotFlow = MutableStateFlow<Pilot?>(null)

    val flights: LiveData<List<FlightAndScenario>> = pilotFlow.flatMapLatest { p: Pilot? ->
        if (p != null) {
            repository.getFlightsAndScenarios(p.pUid)
        } else {
            flowOf(emptyList())
        }
    }.asLiveData()


    var flight: Flight? = null
    val activeScenario: LiveData<Scenario> = repository.activeScenario

    private val _openFlightLaunchDialog = MutableLiveData(false)
    val openFlightLaunchDialog: LiveData<Boolean>
        get() = _openFlightLaunchDialog

    private val _openFlightEditDialog = MutableLiveData(false)
    val openFlightEditDialog: LiveData<Boolean>
        get() = _openFlightEditDialog

    private val _navigateUp = MutableLiveData(false)
    val navigateUp: LiveData<Boolean>
        get() = _navigateUp

    fun navigateUp() {
        _navigateUp.value = true
    }

    fun openFlightLaunchDialog() {
        _openFlightLaunchDialog.value = true
    }

    fun flightLaunchDialogDone() {
        _openFlightLaunchDialog.value = false
    }

    fun openFlightEditDialog() {
        _openFlightEditDialog.value = true
    }

    fun flightEditDialogDone() {
        _openFlightEditDialog.value = false
    }

    fun launchFlight() {
        applicationScope.launch {
            repository.launch(
                Flight(
                    pilotFlow.value!!,
                    startTime = OffsetDateTime.now(),
                )
            )
        }
    }

    fun deleteFlights(uuids: Set<UUID>) {
        applicationScope.launch {
            repository.deleteFlights(uuids)
        }

    }

    fun saveFlight() {
        applicationScope.launch {
            repository.update(flight!!)
        }
    }

    fun setActiveScenarioToFlights(uuids: Set<UUID>) {
        applicationScope.launch {
            repository.setActiveScenarioToFlights(uuids)
        }
    }

}
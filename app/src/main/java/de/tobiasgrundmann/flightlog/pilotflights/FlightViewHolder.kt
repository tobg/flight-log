/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.pilotflights

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import de.tobiasgrundmann.flightlog.R
import de.tobiasgrundmann.flightlog.database.FlightAndScenario
import de.tobiasgrundmann.flightlog.extensions.dateOrTimeTodayFormatted
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class FlightViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val flightButton: Button = itemView.findViewById(R.id.flightlist_flight)
    private val clipForeground: View = itemView.findViewById(R.id.clipForegroundFlight)
//    val clipBackgroundActivate: View = itemView.findViewById(R.id.clipBackgroundFlightActivate)
//    val clipBackgroundDeactivate: View = itemView.findViewById(R.id.clipBackgroundFlightDeactivate)
//    val clipBackgroundEdit: View = itemView.findViewById(R.id.clipBackgroundFlightEdit)

    companion object {
        fun create(
            parent: ViewGroup
        ): FlightViewHolder {
            val view: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.recyclerview_flightlist_item, parent, false)

            return FlightViewHolder(view)
        }
    }

    fun bind(flightAndScenario: FlightAndScenario, adapter: FlightlistAdapter) {
        clipForeground.isActivated = true
        clipForeground.isSelected = adapter.isSelected(flightAndScenario.flight.fUid)

        flightButton.setOnClickListener {
            if (adapter.isActionMode()) {
                adapter.onClick(flightAndScenario.flight.fUid)
            } else {
                adapter.viewModel.flight = flightAndScenario.flight
                adapter.viewModel.openFlightEditDialog()
                adapter.notifyItemChanged(adapterPosition)
            }
        }
        flightButton.setOnLongClickListener {
            //Timber.d("Long Click Button")
            adapter.onLongClick(flightAndScenario.flight.fUid)
            true
        }

        with(flightAndScenario.flight.startTime) {
            val formattedScenario: String = flightAndScenario.scenario?.let { scenario ->
                if (scenario.name.isNotBlank()) {
                        "/ " + scenario.name
                } else ""
            } ?: ""
            val formattedTime = this.dateOrTimeTodayFormatted()
            val text = clipForeground.context.getString(R.string.flight_list_item,formattedTime,formattedScenario)
            flightButton.text = text
        }
        val flightSymbol = if (flightAndScenario.flight.landingConfirmed) {
            AppCompatResources.getDrawable(flightButton.context,R.drawable.ic_baseline_download_done_24 )
        } else {
            AppCompatResources.getDrawable(flightButton.context,R.drawable.ic_baseline_flight_24 )
        }
        flightButton.setCompoundDrawablesWithIntrinsicBounds(flightSymbol,null,null,null)
    }
}
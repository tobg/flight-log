/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog

import android.content.res.Resources

enum class MessageId(private val resId: Int) {
    NO_MESSAGE(-1),
    IMPORT_ERROR(R.string.import_error),
    CONFIGURE_IMPORT_URL(R.string.configure_import_url),
    PILOT_WITH_CUSTOMER_ID_EXISTS(R.string.pilot_with_customerId_exists),
    SCENARIO_WITH_NAME_EXISTS(R.string.scenario_with_name_exists);

    fun getString(res: Resources, formatArgs: Array<Any> = arrayOf()) =
        res.getString(this.resId, *formatArgs)

}

data class MessageToUser(
    private val key: MessageId,
    private val formatArgs: Array<Any> = arrayOf()
) {
    fun hasMessage() =
        key != MessageId.NO_MESSAGE

    fun getString(res: Resources) = key.getString(res, formatArgs)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MessageToUser

        if (key != other.key) return false
        if (!formatArgs.contentEquals(other.formatArgs)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = key.hashCode()
        result = 31 * result + formatArgs.contentHashCode()
        return result
    }

    companion object {
        val noMessage = MessageToUser(MessageId.NO_MESSAGE)
    }
}
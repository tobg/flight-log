/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.pilotlist

import android.app.Dialog
import android.os.Bundle
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import de.tobiasgrundmann.flightlog.MainActivity
import de.tobiasgrundmann.flightlog.R
import de.tobiasgrundmann.flightlog.database.Pilot
import timber.log.Timber

class PilotEditDialog(val pilot: Pilot, val onOK: () -> Unit) : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val mainActivity = requireActivity() as MainActivity
        return mainActivity.let {
            val builder = MaterialAlertDialogBuilder(it)

            val inflater = it.layoutInflater
            val view = inflater.inflate(R.layout.dialog_edit_pilot, null)
            val name: TextView = view.findViewById(R.id.textinput_name_text)
            val lastname: TextView = view.findViewById(R.id.textinput_lastname_text)
            val weight: TextView = view.findViewById(R.id.textinput_weight_text)
            val status: TextView = view.findViewById(R.id.textinput_status_text)
            val vehicle: TextView = view.findViewById(R.id.textinput_vehicle_text)
            val customerId: TextView = view.findViewById(R.id.textinput_customer_id_text)

            name.text = pilot.firstname
            lastname.text = pilot.lastname
            if (pilot.weight != 0) {
                weight.text = pilot.weight.toString()
            }
            status.text = pilot.status
            vehicle.text = pilot.currentFlightVehicle
            customerId.text = pilot.customerID

            builder.setView(view)
                .setPositiveButton(
                    R.string.ok
                ) { _, _ ->
                    Timber.d("Name: $name Lastname: $lastname Weight: $weight")
                    pilot.firstname = name.text.toString()
                    pilot.lastname = lastname.text.toString()
                    if (weight.text.isNotBlank()) {
                        try {
                            pilot.weight = Integer.parseInt(weight.text.toString())
                        } catch (e: Exception) {
                            pilot.weight = 0
                        }
                    } else {
                        pilot.weight = 0
                    }
                    pilot.status = status.text.toString()
                    pilot.currentFlightVehicle = vehicle.text.toString()
                    val customerIdStr = customerId.text.toString()
                    //Timber.d("Setting customerId ${customerIdStr}")
                    pilot.customerID = customerIdStr.ifBlank {
                        //Timber.d("Setting customerId to null")
                        null
                    }
                    onOK()
                }
                .setNegativeButton(
                    R.string.cancel
                ) { dialog, _ ->
                    dialog.cancel()
                }
            builder.create()
        }
    }
}
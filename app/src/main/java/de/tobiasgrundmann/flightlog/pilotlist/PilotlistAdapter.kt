/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.pilotlist

import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import de.tobiasgrundmann.flightlog.R
import de.tobiasgrundmann.flightlog.common.*
import de.tobiasgrundmann.flightlog.database.Pilot
import timber.log.Timber
import java.util.*

class PilotlistAdapter(
    val viewModel: PilotlistViewModel,
    recyclerview: RecyclerView
) : ActivatablesAdapter<Pilot, SwipeForActivationViewHolder>(recyclerview),
    SearchView.OnQueryTextListener,
    SearchView.OnCloseListener {

    var searchText: String = ""

    private val myActionModeCallback = object : ActionMode.Callback {
        override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
            val inflater: MenuInflater = mode.menuInflater
            inflater.inflate(R.menu.cab_menu, menu)
            return true
        }

        override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
            Timber.d("onPrepareActionMode")
            return false
        }

        override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
            return when (item.itemId) {
                R.id.menu_select_all -> {
                    selectAllOrNothing()
                    true
                }
                R.id.menu_delete -> {
                    val selectedUuids = getSelection()
                    MaterialAlertDialogBuilder(recyclerview.context)
                        .setTitle(recyclerview.context.getString(R.string.delete_pilots_title))
                        .setMessage(

                            recyclerview.context.resources.getQuantityString(
                                R.plurals.delete_pilots_msg,
                                selectedUuids.size,
                                selectedUuids.size
                            )
                        )
                        .setPositiveButton(
                            R.string.ok
                        ) { _, _ ->
                            getAndResetSelection()
                            viewModel.deletePilots(selectedUuids)
                            mode.finish()
                        }
                        .setNegativeButton(
                            R.string.cancel
                        ) { dialog, _ ->
                            dialog.cancel()
                        }
                        .show()
                    true
                }
                R.id.menu_activate -> {
                    viewModel.setActive(getAndResetSelection(), true)
                    mode.finish()
                    true
                }
                R.id.menu_deactivate -> {
                    viewModel.setActive(getAndResetSelection(), false)
                    mode.finish()
                    true
                }
                R.id.menu_export_flights -> {
                    viewModel.openExportDialog(getAndResetSelection())
                    mode.finish()
                    true
                }
                R.id.menu_export_pilots -> {
                    viewModel.exportPilots(getAndResetSelection())
                    mode.finish()
                    true
                }

                else -> false
            }
        }

        override fun onDestroyActionMode(mode: ActionMode) {
            destroyActionMode()
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SwipeForActivationViewHolder {
        return when (viewType) {
            ItemType.HEADER -> HeaderViewHolder.create(parent)
            ItemType.ITEM -> PilotViewHolder.create(parent)
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun onViewRecycled(holder: SwipeForActivationViewHolder) {
        super.onViewRecycled(holder)
        holder.itemView.isActivated = false
    }

    override fun onBindViewHolder(holder: SwipeForActivationViewHolder, position: Int) {
        when (holder) {
            is HeaderViewHolder -> {
                val thing = getItem(position) as ActivatableHeader
                holder.bind(thing.active)
            }
            is PilotViewHolder -> {
                val thing = getItem(position)
                holder.bind(thing as Pilot, this)
            }
        }
    }

    private fun updateSearch(searchText: String) {
        this.searchText = searchText
        viewModel.setSearchText(searchText)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        updateSearch(newText ?: "")
        return true
    }

    override fun onClose(): Boolean {
        updateSearch("")
        return true
    }

    override fun getPosition(uid: UUID) = displayList.indexOfFirst { it.uuid() == uid }

    override fun allowSwipe() = !isActionMode()

    override fun getActionModeCallback(): ActionMode.Callback {
        return myActionModeCallback
    }

    override fun openEditDialog(activatable: Activatable) {
        viewModel.pilot = activatable as Pilot
        viewModel.openPilotEditDialog(new = false)
    }

    override fun toggleActivation(activatable: Activatable) {
        activatable.toggleActivation()
        viewModel.update(activatable as Pilot)
    }
}

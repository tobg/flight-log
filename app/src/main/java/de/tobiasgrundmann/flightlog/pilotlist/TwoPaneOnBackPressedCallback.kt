/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.pilotlist

import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.core.view.doOnLayout
import androidx.slidingpanelayout.widget.SlidingPaneLayout
import timber.log.Timber

class TwoPaneOnBackPressedCallback(
    private val slidingPaneLayout: SlidingPaneLayout,
    private val viewModel: PilotlistViewModel,
) : OnBackPressedCallback(
    false
), SlidingPaneLayout.PanelSlideListener {

    init {
        slidingPaneLayout.addPanelSlideListener(this)
        slidingPaneLayout.doOnLayout {
            syncState()
        }
    }

    private fun syncState() {
        slidingPaneLayout.let {
            //Timber.d("doOnLayout:  open: ${it.isOpen} slideable: ${it.isSlideable}")
            isEnabled = it.isSlideable && it.isOpen
            viewModel.drawNav.value = isEnabled
        }
    }

    override fun handleOnBackPressed() {
        //Timber.d("handleOnBackPressed")
        slidingPaneLayout.closePane()
    }

    override fun onPanelOpened(panel: View) {
        //Timber.d("panel opened")
        syncState()
    }

    override fun onPanelClosed(panel: View) {
        Timber.d("panel closed")
        syncState()
        viewModel.detailViewModel.pilotFlow.value = null
    }

    override fun onPanelSlide(panel: View, slideOffset: Float) {}

}

/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.pilotlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import de.tobiasgrundmann.flightlog.FlightLogApplication
import de.tobiasgrundmann.flightlog.MainActivity
import de.tobiasgrundmann.flightlog.pilotflights.PilotFlightsViewModel
import de.tobiasgrundmann.flightlog.pilotflights.PilotFlightsViewModelFactory

class PilotlistViewModelFactory(
    private val fragment: PilotlistFragment
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PilotlistViewModel::class.java)) {
            val flightLogApplication =
                fragment.requireActivity().application as FlightLogApplication
            val detailVMFactory = PilotFlightsViewModelFactory(
                flightLogApplication.repository,
                flightLogApplication.applicationScope
            )
            val detailViewModel =
                ViewModelProvider(fragment, detailVMFactory).get(PilotFlightsViewModel::class.java)
            val userMessage = (fragment.requireActivity() as MainActivity).userMessage
            @Suppress("UNCHECKED_CAST")
            return PilotlistViewModel(
                detailViewModel,
                flightLogApplication.repository,
                flightLogApplication.applicationScope,
                userMessage
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
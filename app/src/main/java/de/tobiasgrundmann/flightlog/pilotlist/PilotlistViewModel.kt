/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.pilotlist

import androidx.lifecycle.*
import de.tobiasgrundmann.flightlog.MessageId
import de.tobiasgrundmann.flightlog.MessageToUser
import de.tobiasgrundmann.flightlog.database.FlightLogRepository
import de.tobiasgrundmann.flightlog.database.Pilot
import de.tobiasgrundmann.flightlog.extensions.dateOrTimeTodayFormatted
import de.tobiasgrundmann.flightlog.pilotflights.PilotFlightsViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import java.time.LocalDate
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

class PilotlistViewModel(
    val detailViewModel: PilotFlightsViewModel,
    private val repository: FlightLogRepository,
    private val applicationScope: CoroutineScope,
    private val userMessage: MutableLiveData<MessageToUser>
) : ViewModel() {

    private val searchText = MutableStateFlow("")

    private val _openPilotEditDialog = MutableLiveData(false)
    val openPilotEditDialog: LiveData<Boolean>
        get() = _openPilotEditDialog

    private val _openExportDialog = MutableLiveData(false)
    val openExportDialog: LiveData<Boolean>
        get() = _openExportDialog

    private val _exportPilots = MutableLiveData(false)
    val exportPilots: LiveData<Boolean>
        get() = _exportPilots

    private val _navigateToPilotFlightsFragment = MutableLiveData(false)
    val navigateToPilotFlightsFragment: LiveData<Boolean>
        get() = _navigateToPilotFlightsFragment

    val drawNav = MutableStateFlow(false)

    @OptIn(ExperimentalCoroutinesApi::class)
    val pilots: LiveData<List<Pilot>> =
        searchText.flatMapLatest { sText ->
            //Timber.d("search text [$sText]")
            if ("" == sText) {
                repository.pilots
            } else {
                repository.pilots(sText)
            }
        }.asLiveData()


    var lastStartedPilot: LiveData<String> = repository.lastStartedPilot.asLiveData().map { map ->
        if (map.isNotEmpty()) {
            map.keys.first().firstname + " " + map.values.first().startTime.dateOrTimeTodayFormatted()
        } else {
            ""
        }
    }

    var pilot = Pilot()
    private var newPilot: Boolean = false
    val selectedPilots: MutableSet<UUID> = mutableSetOf()

    private fun dayOrTimeFormatter(dateTime: OffsetDateTime): DateTimeFormatter {
        return if (dateTime.toLocalDate().equals(LocalDate.now())) {
            DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)
        } else {
            DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
        }
    }

    fun update(pilot: Pilot) = applicationScope.launch {
        repository.update(pilot)
    }

    fun savePilot() {
        Timber.d("savePilot $pilot new:$newPilot")
        if (pilot.lastname.isBlank() && pilot.firstname.isBlank()) {
            return
        }
        applicationScope.launch {
            if (newPilot) {
                if (repository.insert(pilot) == FlightLogRepository.Result.PILOT_WITH_CUSTOMER_ID_EXISTS) {
                    userMessage.postValue(MessageToUser(MessageId.PILOT_WITH_CUSTOMER_ID_EXISTS))
                }
            } else {
                repository.update(pilot)
            }
            newPilot = false
        }
    }

    fun setSearchText(text: String) {
        searchText.value = text
    }

    fun openPilotEditDialog(new: Boolean) {
        newPilot = new
        if (new) {
            pilot = Pilot("", searchText.value)
        }
        _openPilotEditDialog.value = true
    }

    fun pilotEditDialogDone() {
        _openPilotEditDialog.value = false
    }

    fun openExportDialog(selected: Set<UUID>) {
        selectedPilots.clear()
        selectedPilots.addAll(selected)
        _openExportDialog.value = true
    }

    fun exportDialogDone() {
        _openExportDialog.value = false
    }

    fun navigateToPilotFlightsFragment() {
        _navigateToPilotFlightsFragment.value = true
    }

    fun doneNavigation() {
        _navigateToPilotFlightsFragment.value = false
    }

    fun setActive(uuids: Set<UUID>, status: Boolean) {
        //Timber.d("setActive $status $uuids")
        applicationScope.launch {
            repository.setActive(uuids, status)
        }
    }

    fun deletePilots(uuids: Set<UUID>) {
        //Timber.d("deletePilots $uuids")
        val pilot = detailViewModel.pilotFlow.value
        if (pilot != null && uuids.contains(pilot.uuid())) {
            detailViewModel.pilotFlow.value = null
        }
        applicationScope.launch {
            repository.deletePilots(uuids)
        }
    }

    fun exportPilots(selected: Set<UUID>) {
        selectedPilots.clear()
        selectedPilots.addAll(selected)
        _exportPilots.value = true
    }

    fun exportPilotsDone() {
        _exportPilots.value = false
    }
}
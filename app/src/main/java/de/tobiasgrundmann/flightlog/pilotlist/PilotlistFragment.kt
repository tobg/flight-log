/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.pilotlist

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.core.view.doOnNextLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.asLiveData
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.slidingpanelayout.widget.SlidingPaneLayout
import de.tobiasgrundmann.flightlog.MainActivity
import de.tobiasgrundmann.flightlog.R
import de.tobiasgrundmann.flightlog.common.SwipeForActivationTouchCallback
import de.tobiasgrundmann.flightlog.databinding.FragmentPilotlistBinding
import timber.log.Timber

class PilotlistFragment : Fragment() {
    private var pilotsAdapter: PilotlistAdapter? = null
    private val viewModelFactory =
        PilotlistViewModelFactory(this)
    private val viewModel: PilotlistViewModel by activityViewModels { viewModelFactory }
    private lateinit var backPressHandler: TwoPaneOnBackPressedCallback
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        //Timber.d("onCreateViewModel")

        val binding: FragmentPilotlistBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_pilotlist, container, false
        )

        binding.slidingPaneLayout.lockMode = SlidingPaneLayout.LOCK_MODE_LOCKED_CLOSED
        backPressHandler = TwoPaneOnBackPressedCallback(binding.slidingPaneLayout, viewModel)
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            backPressHandler
        )
        val adapter = PilotlistAdapter(viewModel, binding.recyclerviewPilotlist)
        val touchHelper = ItemTouchHelper(SwipeForActivationTouchCallback(adapter))
        touchHelper.attachToRecyclerView(binding.recyclerviewPilotlist)
        viewModel.pilots.observe(viewLifecycleOwner) { list ->
            //Timber.d("viewModel.pilots.observe")
            adapter.submitListAddHeader(list)
            //initialize with an arbitrary but valid value if opened
            //on a large screen (slidingPane is open) and the user has not yet selected a pilot
            if (viewModel.detailViewModel.pilotFlow.value == null && list.isNotEmpty()) {
                viewModel.detailViewModel.pilotFlow.value = list.first()
            }
        }
        viewModel.detailViewModel.navigateUp.observe(viewLifecycleOwner) {
            if (backPressHandler.isEnabled) {
                backPressHandler.handleOnBackPressed()
            }
        }
        viewModel.drawNav.asLiveData().observe(viewLifecycleOwner) { drawNav ->
            val toolbar = (requireActivity() as MainActivity).binding.toolbar
            val menusEnabled: Boolean
            if (drawNav) {
                toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24)
                toolbar.setNavigationOnClickListener {
                    viewModel.detailViewModel.navigateUp()
                }
                menusEnabled = false
            } else {
                //toolbar.setNavigationOnClickListener(null)
                toolbar.navigationIcon = null
                menusEnabled = true
            }
            //some menus don't make sense and might even be confusing if only the detail pane is open
            toolbar.doOnNextLayout {
                toolbar.menu.findItem(R.id.menu_search)?.isVisible = menusEnabled
                toolbar.menu.findItem(R.id.menu_import)?.isVisible = menusEnabled
                toolbar.menu.findItem(R.id.menu_import_from_server)?.isVisible = menusEnabled
                toolbar.menu.findItem(R.id.menu_export_flights)?.isVisible = menusEnabled
                toolbar.menu.findItem(R.id.menu_export_pilots)?.isVisible = menusEnabled
            }
        }

        viewModel.openPilotEditDialog.observe(viewLifecycleOwner) {
            if (it) {
                val dialogFragment = PilotEditDialog(viewModel.pilot) {
                    viewModel.savePilot()
                }
                dialogFragment.show(parentFragmentManager, "EDIT_PILOT_DIALOG")
                viewModel.pilotEditDialogDone()
            }
        }
        viewModel.openExportDialog.observe(viewLifecycleOwner) { doIt ->
            if (doIt) {
                val dialogFragment = ExportDialog {
                    (requireActivity() as MainActivity).exportFlights(viewModel.selectedPilots, it)
                }
                dialogFragment.show(parentFragmentManager, "EXPORT_DIALOG")
                viewModel.exportDialogDone()
            }
        }

        viewModel.exportPilots.observe(viewLifecycleOwner) {
            if (it) {
                (requireActivity() as MainActivity).exportPilots(viewModel.selectedPilots)
                viewModel.exportPilotsDone()
            }
        }

        viewModel.navigateToPilotFlightsFragment.observe(viewLifecycleOwner) {
            if (it) {
                //Timber.d("setting Pilot ${viewModel.pilot}")
                viewModel.detailViewModel.pilotFlow.value = viewModel.pilot
                binding.slidingPaneLayout.openPane()
                viewModel.doneNavigation()
            }
        }

        setHasOptionsMenu(true)

        this.pilotsAdapter = adapter
        binding.pilotlistViewModel = viewModel
        binding.recyclerviewPilotlist.adapter = adapter
        binding.recyclerviewPilotlist.layoutManager = LinearLayoutManager(this.requireContext())
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root

    }

//    override fun onResume() {
//        Timber.d("onResume")
//        super.onResume()
//    }
//
//    override fun onStart() {
//        Timber.d("onStart")
//        super.onStart()
//    }
//
//    override fun onDestroy() {
//        Timber.d("onDestroy")
//        super.onDestroy()
//    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_import -> {
                Timber.d("import")
                (requireActivity() as MainActivity).importPilotsLocal()
                true
            }
            R.id.menu_import_from_server -> {
                Timber.d("import")
                (requireActivity() as MainActivity).importPilotsFromServer()
                true
            }
            R.id.menu_export_pilots -> {
                Timber.d("exportPilots")
                viewModel.exportPilots(emptySet())
                true
            }
            R.id.menu_export_flights -> {
                Timber.d("export flights")
                viewModel.openExportDialog(emptySet())
                true
            }
            else -> {
                //Timber.d("navigate to $item")
                return NavigationUI.onNavDestinationSelected(
                    item,
                    requireView().findNavController()
                )
                        || super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val searchView = menu.findItem(R.id.menu_search).actionView as SearchView
        searchView.setOnQueryTextListener(pilotsAdapter)
        searchView.setOnCloseListener(pilotsAdapter)
    }

}
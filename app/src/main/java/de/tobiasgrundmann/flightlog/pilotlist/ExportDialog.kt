/*
 * Copyright (c) 2022.
 *
 * This file is part of Flight Log.
 *
 * Flight Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Flight Log is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with Flight Log. If not, see <https://www.gnu.org/licenses/>.
 */

package de.tobiasgrundmann.flightlog.pilotlist

import android.app.Dialog
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import androidx.fragment.app.DialogFragment
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointBackward
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import de.tobiasgrundmann.flightlog.MainActivity
import de.tobiasgrundmann.flightlog.R
import java.time.Instant
import java.time.OffsetDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class ExportDialog(val onOK: (Pair<OffsetDateTime?, OffsetDateTime?>) -> Unit) : DialogFragment() {
    companion object {
        private val dateMedium = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val mainActivity = requireActivity() as MainActivity
        return mainActivity.let {
            val builder = MaterialAlertDialogBuilder(it)
            val inflater = it.layoutInflater
            val view = inflater.inflate(R.layout.dialog_export, null)
            val selectRangeBt: Button = view.findViewById(R.id.button_select_range)
            val selectAllCheckBox: CheckBox = view.findViewById(R.id.checkBox_select_all_dates)
            var rangeStart: OffsetDateTime? = null
            var rangeEnd: OffsetDateTime? = null

            selectAllCheckBox.isChecked = true
            selectAllCheckBox.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    rangeStart = null
                    rangeEnd = null
                    selectRangeBt.text = getString(R.string.select_dates)
                } else {
                    if (rangeStart == null) {
                        selectRangeBt.text = getString(R.string.select_dates)
                    }
                }
            }
            val calendarConstraints = CalendarConstraints.Builder()
                .setValidator(DateValidatorPointBackward.now())
                .build()

            val dateRangePicker =
                MaterialDatePicker.Builder
                    .dateRangePicker()
                    .setCalendarConstraints(calendarConstraints)
                    .setTitleText(getString(R.string.select_dates))
                    .setSelection(
                        androidx.core.util.Pair(
                            MaterialDatePicker.thisMonthInUtcMilliseconds(),
                            MaterialDatePicker.todayInUtcMilliseconds()
                        )
                    )
                    .build()
            dateRangePicker.addOnPositiveButtonClickListener { pair ->
                val startInstant = Instant.ofEpochMilli(pair.first)
                rangeStart = OffsetDateTime.ofInstant(startInstant, ZoneId.systemDefault())
                val endInstant = Instant.ofEpochMilli(pair.second)
                rangeEnd = OffsetDateTime.ofInstant(endInstant, ZoneId.systemDefault())
                selectAllCheckBox.isChecked = false
                selectRangeBt.text = getString(
                    R.string.date_range,
                    rangeStart!!.toLocalDate().format(dateMedium),
                    rangeEnd!!.toLocalDate().format(dateMedium)
                )
            }
            selectRangeBt.setOnClickListener {
                dateRangePicker.show(parentFragmentManager, "EXPORT_DATE_RANGE_PICKER")
            }

            builder.setView(view)
                .setPositiveButton(
                    R.string.export_flights
                ) { _, _ ->
                    onOK(Pair(rangeStart, rangeEnd))
                }
                .setNegativeButton(
                    R.string.cancel
                ) { dialog, _ ->
                    dialog.cancel()
                }
            builder.create()
        }
    }

}